-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: hua-hin_test
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `error_log`
--

DROP TABLE IF EXISTS `error_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `error_log` (
  `Location` varchar(45) NOT NULL,
  `Plant` varchar(45) NOT NULL,
  `Tank` int(11) NOT NULL,
  `Details` varchar(45) NOT NULL,
  `Time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `error_log`
--

LOCK TABLES `error_log` WRITE;
/*!40000 ALTER TABLE `error_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `error_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materail_master`
--

DROP TABLE IF EXISTS `materail_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `materail_master` (
  `TankName` varchar(45) NOT NULL,
  `MaterailName` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materail_master`
--

LOCK TABLES `materail_master` WRITE;
/*!40000 ALTER TABLE `materail_master` DISABLE KEYS */;
INSERT INTO `materail_master` VALUES ('Tank1','Materail_1'),('Tank2','Materail_2'),('Tank3','Materail_3'),('Tank4','Materail_4'),('Tank5','Materail_5'),('Tank6','Materail_6'),('Tank7','Materail_7'),('Tank8','Materail_8'),('Tank9','Materail_9'),('Tank10','Materail_10'),('Tank11','Materail_11'),('Tank12','Materail_12'),('Tank13','Materail_13'),('Tank14','Materail_14'),('Tank15','Materail_15'),('Tank16','Materail_16'),('Tank17','Materail_17'),('Tank18','Materail_18'),('Tank19','Materail_19'),('Tank20','Materail_20');
/*!40000 ALTER TABLE `materail_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material_command`
--

DROP TABLE IF EXISTS `material_command`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `material_command` (
  `ID` int(11) NOT NULL,
  `OrderNumber` varchar(45) NOT NULL,
  `TankName` varchar(45) NOT NULL,
  `Values` varchar(45) NOT NULL,
  `ProcessStart` varchar(45) DEFAULT NULL,
  `ProcessStop` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material_command`
--

LOCK TABLES `material_command` WRITE;
/*!40000 ALTER TABLE `material_command` DISABLE KEYS */;
INSERT INTO `material_command` VALUES (650,'1000','Tank1','100',NULL,NULL),(650,'1000','Tank2','150',NULL,NULL),(650,'1000','Tank3','250',NULL,NULL),(651,'1001','Tank2','125',NULL,NULL),(651,'1001','Tank3','125',NULL,NULL),(651,'1001','Tank4','125',NULL,NULL),(652,'1002','Tank3','125',NULL,NULL),(652,'1002','Tank4','125',NULL,NULL),(652,'1002','Tank5','125',NULL,NULL),(653,'1003','Tank4','125',NULL,NULL),(653,'1003','Tank5','125',NULL,NULL),(653,'1003','Tank6','125',NULL,NULL),(654,'1000','Tank1','100',NULL,NULL),(654,'1000','Tank2','150',NULL,NULL),(654,'1000','Tank3','250',NULL,NULL),(655,'1001','Tank2','125',NULL,NULL),(655,'1001','Tank3','125',NULL,NULL),(655,'1001','Tank4','125',NULL,NULL),(656,'1002','Tank3','125',NULL,NULL),(656,'1002','Tank4','125',NULL,NULL),(656,'1002','Tank5','125',NULL,NULL),(657,'1003','Tank4','125',NULL,NULL),(657,'1003','Tank5','125',NULL,NULL),(657,'1003','Tank6','125',NULL,NULL);
/*!40000 ALTER TABLE `material_command` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productionorder`
--

DROP TABLE IF EXISTS `productionorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productionorder` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Location` varchar(45) NOT NULL,
  `Plant` varchar(45) NOT NULL,
  `OrderNumber` varchar(45) NOT NULL,
  `ProcessStart` datetime DEFAULT NULL,
  `ProcessStop` datetime DEFAULT NULL,
  `GenXML` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=658 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productionorder`
--

LOCK TABLES `productionorder` WRITE;
/*!40000 ALTER TABLE `productionorder` DISABLE KEYS */;
INSERT INTO `productionorder` VALUES (650,'ZoneA','A','1000',NULL,NULL,NULL),(651,'ZoneA','A','1001',NULL,NULL,NULL),(652,'ZoneA','B','1002',NULL,NULL,NULL),(653,'ZoneA','B','1003',NULL,NULL,NULL),(654,'ZoneA','A','1000',NULL,NULL,NULL),(655,'ZoneA','A','1001',NULL,NULL,NULL),(656,'ZoneA','B','1002',NULL,NULL,NULL),(657,'ZoneA','B','1003',NULL,NULL,NULL);
/*!40000 ALTER TABLE `productionorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productionorder_master`
--

DROP TABLE IF EXISTS `productionorder_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productionorder_master` (
  `ProductionNumber` varchar(45) NOT NULL,
  `MaterailName` varchar(45) NOT NULL,
  `Values` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productionorder_master`
--

LOCK TABLES `productionorder_master` WRITE;
/*!40000 ALTER TABLE `productionorder_master` DISABLE KEYS */;
INSERT INTO `productionorder_master` VALUES ('1000','Materail_1','100'),('1000','Materail_2','150'),('1000','Materail_3','250'),('1001','Materail_2','125'),('1001','Materail_3','125'),('1001','Materail_4','125'),('1002','Materail_3','125'),('1002','Materail_4','125'),('1002','Materail_5','125'),('1003','Materail_4','125'),('1003','Materail_5','125'),('1003','Materail_6','125'),('1004','Materail_5','125'),('1004','Materail_6','125'),('1004','Materail_7','125');
/*!40000 ALTER TABLE `productionorder_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-22 16:22:46
