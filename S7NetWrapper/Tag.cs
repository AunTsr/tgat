﻿#region Using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
#endregion

namespace S7NetWrapper
{
    public class Tag
    {
        private string _itemName;
        public string ItemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }

        private object _itemValue;
        public object ItemValue
        {
            get { return _itemValue; }
            set { _itemValue = value; }
        }

        public string Datatype {get; set;}

        public Tag()
        {
            
        }

        public Tag(string itemName) 
        {
            this.ItemName = itemName;
        }
        public Tag(string itemName, object itemValue)
        {
            this.ItemName = itemName;
            this.ItemValue = itemValue;
        }

        public Tag(string itemName, object itemValue, string Datatype) 
        {
            this.ItemName = itemName;
            this.ItemValue = itemValue;
            this.Datatype = Datatype;
        }
    }

    public class TagsName2Datatype
    {
        public int Db { get; set; }
        public int StartAdress { get; set; }
        public int Count { get; set; }
    }

    public enum DataType
    {
        Undefine,
        Bool,
        Int,
        String,
        Float
    }
}
