﻿using S7.Net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace S7NetWrapper
{
    //public class Plc : plcaddress {
    //    public Plc()
    //    {

    //    }
    //}

    public class S7NetPlcDriver : IPlcSyncDriver
    {
        #region Private fields

        S7.Net.Plc client;
        //TagsName2Datatype Adress;

        #endregion     

        #region Constructor

        public S7NetPlcDriver(CpuType cpu, string ip, short rack, short slot)
        {
            client = new S7.Net.Plc(cpu, ip, rack, slot);
        } 

        #endregion

        #region IPlcSyncDriver members

        private ConnectionStates _connectionState;
        public ConnectionStates ConnectionState
        {
            get { return _connectionState; }
            private set { _connectionState = value; }
        }

        public void Connect()
        {
            ConnectionState = ConnectionStates.Connecting;
            client.Open();
            //var error = client.Open();
            //if (error != ErrorCode.NoError)
            //{
            //    ConnectionState = ConnectionStates.Offline;
            //    throw new Exception(error.ToString());
            //}
            if(client.IsConnected==true && client.IsAvailable== true)
            ConnectionState = ConnectionStates.Online;
            else ConnectionState = ConnectionStates.Offline;
        }

        public void Disconnect()
        {
            ConnectionState = ConnectionStates.Offline;
            client.Close();
        }        

        public List<Tag> ReadItems(List<Tag> itemList)
        {
            if (this.ConnectionState != ConnectionStates.Online)
            {
                throw new Exception("Can't read, the client is disconnected.");
            }

            //List<Tag> tags = new List<Tag>();
            foreach (var item in itemList)
            {
                try
                {
                    //Debug.Print(item.ItemName);
                    var result = new object();
                    //var adr = new PLCAddress(variable);
                    //return Read(adr.DataType, adr.DbNumber, adr.StartByte, adr.VarType, 1, (byte)adr.BitNumber);
                    var Adress = Con2Adress(item.ItemName);
                    switch (Conv2DataType(item.Datatype))
                    {
                        case DataType.Float:
                            //Con2Adress(item.ItemName);
                            result = client.Read(S7.Net.DataType.DataBlock, Adress.Db, Adress.StartAdress, VarType.Real, Adress.Count);
                            break;
                        case DataType.String:
                            //Con2Adress(item.ItemName);
                            result = client.Read(S7.Net.DataType.DataBlock, Adress.Db, Adress.StartAdress, VarType.String, Adress.Count);
                            break;
                        //case DataType.Int:
                        //    Con2Adress(item.ItemName);
                        //    byte[] quality = client.ReadBytes(S7.Net.DataType.DataBlock, Adress.Db, Adress.StartAdress, 2);
                        //    string[] arrStatus = ((IEnumerable)quality).Cast<object>().Select(x => x.ToString()).ToArray();
                        //    result = quality[0]*256 + quality[1];
                        //    break;
                        default:
                            try
                            {
                                result = client.Read(item.ItemName);
                            }
                            catch
                            {
                                result = item.ItemValue;
                            }
                            break;
                    }

                    //Tag tag = new Tag(item.ItemName);

                    if (result is ErrorCode && (ErrorCode)result != ErrorCode.NoError)
                    {
                        //throw new Exception(((ErrorCode)result).ToString() + "\n" + "Tag: " + tag.ItemName);
                        item.ItemValue = null;
                    }
                    else
                    {
                        item.ItemValue = result;
                    }
                }
                catch (Exception)
                {

                }
                
                //tag.ItemValue = result;
                //tags.Add(tag);
                //item.ItemValue = result;
            }
            return itemList;
        }

        public void WriteItems(List<Tag> itemList)
        {
            if (this.ConnectionState != ConnectionStates.Online)
            {
                throw new Exception("Can't write, the client is disconnected.");
            }

            foreach (var tag in itemList)
            {
                object value = tag.ItemValue;
                switch (Conv2DataType(tag.Datatype))
                {                    
                    case DataType.Float:
                        var bytes = S7.Net.Types.Double.ToByteArray((double)tag.ItemValue);
                        value = S7.Net.Types.DWord.FromByteArray(bytes);
                        client.Write(tag.ItemName, value);
                        break;
                    case DataType.String:
                        var Adress = Con2Adress(tag.ItemName);
                        client.Write(S7.Net.DataType.DataBlock, Adress.Db, Adress.StartAdress, tag.ItemValue);
                        break;
                    case DataType.Bool:
                        value = tag.ItemValue;
                        client.Write(tag.ItemName, value);
                        break;
                    default:
                        client.Write(tag.ItemName, value);
                        break;
                }            

                        //////////////if (tag.ItemValue is string)
                        //////////////{
                        //////////////    var Adress = Con2Adress(tag.ItemName);
                        //////////////    client.Write(S7.Net.DataType.DataBlock, Adress.Db, Adress.StartAdress, tag.ItemValue);
                        //////////////}
                        //////////////else
                        //////////////{
                        //////////////    object value = tag.ItemValue;
                        //////////////    if (tag.ItemValue is double)
                        //////////////    {
                        //////////////        var bytes = S7.Net.Types.Double.ToByteArray((double)tag.ItemValue);
                        //////////////        value = S7.Net.Types.DWord.FromByteArray(bytes);
                        //////////////    }

                        //////////////    else if (tag.ItemValue is bool)
                        //////////////    {
                        //////////////        value = (bool)tag.ItemValue ? 1 : 0;
                        //////////////    }

                        //////////////    client.Write(tag.ItemName, value);
                        //////////////}

                        //var result = client.Write(tag.ItemName, value);
                        //if (result is ErrorCode && (ErrorCode)result != ErrorCode.NoError)
                        //{
                        //    throw new Exception(((ErrorCode)result).ToString() + "\n" + "Tag: " + tag.ItemName);
                        //}
            }
        }

        public void ReadClass(object sourceClass, int db)
        {
            client.ReadClass(sourceClass, db);
        }

        public void WriteClass(object sourceClass, int db)
        {
            client.WriteClass(sourceClass, db);
        }

        private DataType Conv2DataType(string Datatype)
        {
            switch (Datatype)
            {
                case "Bool":
                    return DataType.Bool;
                case "Int":
                    return DataType.Int;
                case "String":
                    return DataType.String;
                case "Float":
                    return DataType.Float;
                default:
                    return DataType.Undefine;
            }
        }

        public TagsName2Datatype Con2Adress(string TagName)
        {
            var Adress = new S7NetWrapper.TagsName2Datatype();
            string[] sPath = TagName.Split('.');
            if (sPath.Length == 2)
            {
                Adress.Db = Convert.ToInt32(sPath[0].Trim('D','B'));
                Adress.StartAdress = Convert.ToInt32(sPath[1].Trim('D', 'B', 'W', 'X'));
                Adress.Count = 1;
            }
            else if (sPath.Length == 3)
            {
                Adress.Db = Convert.ToInt32(sPath[0].Trim('D', 'B'));
                Adress.StartAdress = Convert.ToInt32(sPath[1].Trim('S', 'T', 'R', 'I', 'N', 'G', 'D', 'B', 'W', 'X')) + 2 ;
                Adress.Count = Convert.ToInt32(sPath[2]);
            }
            return Adress;
        }
        #endregion
    }
}
