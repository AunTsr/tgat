﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Additive_MatDosing_SAPInterface.Model;

namespace Additive_MatDosing_SAPInterface.dal {
    #region Accounts
    //public class dbQuery {
    //    public static bool CreateAccount(AccountModel model) {
    //        var db = dbMng.Connect();
    //        var result = false;
    //        try {
    //            // Get Accounts collection
    //            var account = dbMng.Accounts(db);

    //            if (!account.Exists(x => x.ibans == model.ibans)) {
    //                //model.id = account.Max("_id").AsInt32 + 1;
    //                if (model.money > 0) { model.money = feecharge(model.money); }//fee charge
    //                account.Insert(model);
    //                result = true;
    //            }
    //        }
    //        catch {
    //        }
    //        finally {
    //            var tr = dbMng.Transactions(db);
    //            var trans = new TransactionModel() {
    //                toibans = model.ibans,
    //                amount = model.money,
    //                status = result? Transactionstatus.Success: Transactionstatus.Failed,
    //                transactiontype = Transactiontype.Firstcreate
    //            };
    //            tr.Insert(trans);
    //        }
    //        return result;
    //    }

    //    public static List<AccountModel> GetAccountData(string[] iban) {
    //        using (var db = dbMng.Connect()) {
    //            // Get Accounts collection
    //            var account = dbMng.Accounts(db);
    //            return account.Find(x => iban.Contains(x.ibans) && x.status).ToList();
    //        }
    //    }

    //    public static List<AccountModel> GetAccountDataAll() {

    //        using (var db = dbMng.Connect()) {

    //            // Get Accounts collection
    //            var account = dbMng.Accounts(db);
    //                return account.FindAll().ToList();
    //        }

    //    }

    //    public static List<AccountModel> GetAccountDataBy(string expr) {

    //        //if (expr.Trim() != "") {
    //        //    using (var db = dbMng.Connect()) {

    //        //        // Get Accounts collection
    //        //        var account = dbMng.Accounts(db);
    //        //        return account.FindAll().ToList();
    //        //    }
    //        //}

    //        return null;
    //    }

    //    #endregion

    //    #region Money

    //    const double feechrgfactor = 0.999; //0.1%
    //    public static bool DepositMoney(TransactionModel trans) {
    //        var db = dbMng.Connect();
    //        var result = false;
    //        try {

    //            // Get Accounts collection
    //            var account = dbMng.Accounts(db);
    //            var acc = account.FindOne(a => a.ibans == trans.toibans);

    //            if (acc != null) {
    //                acc.money += feecharge(trans.amount);
    //                account.Update(acc);

    //                trans.status = Transactionstatus.Success;
    //                result = true;
    //            }

    //        }
    //        catch {

    //        }
    //        finally {
    //            var tr = dbMng.Transactions(db);
    //            trans.status = result ? Transactionstatus.Success : Transactionstatus.Failed;
    //            trans.updatedate = DateTime.Now;
    //            tr.Insert(trans);
    //        }

    //        return result;
    //    }

    //    public static bool TransMoney(TransactionModel trans) {
    //        var db = dbMng.Connect();
    //        var result = false;
    //        try {

    //            var account = dbMng.Accounts(db);
    //            var fromacc = account.FindOne(a => a.ibans == trans.fromibans);
    //            var toacc = account.FindOne(a => a.ibans == trans.toibans);

    //            try {
    //                if (fromacc != null && toacc != null &&
    //                        trans.amount < fromacc.money) {

    //                    //transfer money
    //                    var tmpfromacc = new AccountModel();
    //                    tmpfromacc = fromacc;
    //                    tmpfromacc.money -= trans.amount;

    //                    var tmptoacc = new AccountModel();
    //                    tmptoacc = toacc;
    //                    tmptoacc.money += trans.amount;

    //                    account.Update(tmpfromacc);
    //                    account.Update(tmptoacc);
    //                    result = true;
    //                }
    //            }
    //            catch {
    //                //roll back
    //                account.Update(fromacc);
    //                account.Update(toacc);
    //            }
    //        }
    //        catch {

    //        }
    //        finally {
    //            var tr = dbMng.Transactions(db);
    //            trans.status = result ? Transactionstatus.Success : Transactionstatus.Failed;
    //            trans.updatedate = DateTime.Now;
    //            tr.Insert(trans);
    //        }

    //        return result;
    //    }

    //    #endregion

    //    #region Security

    //    public static void InsertToken(TransactionTokenModel tk) {
    //        using (var db = dbMng.Connect()) {

    //            var tbtoken = dbMng.Tokens(db);
    //            tbtoken.Insert(tk);
    //        }
    //    }
    //    public static void UpdateToken(TransactionTokenModel tk) {
    //        using (var db = dbMng.Connect()) {

    //            var tbtoken = dbMng.Tokens(db);
    //            tk.isactive = false;
    //            tk.activatedatetime = DateTime.Now;
    //            tbtoken.Update(tk);
    //        }
    //    }

    //    #endregion

    //    #region ultilities fn.

    //    private static double feecharge(double money) {
    //        if (money > 0)
    //            return Math.Round((money * feechrgfactor),2);
    //        else
    //            return money;
    //    }
    //}
    #endregion

    #region test
    //public class dbQuery
    //{
    //    public static bool CreateAccount(AccountModel model)
    //    {
    //        var db = dbMng.Connect();
    //        var result = false;
    //        try
    //        {
    //            // Get Accounts collection
    //            var account = dbMng.Accounts(db);

    //            if (!account.Exists(x => x.ibans == model.ibans))
    //            {
    //                //model.id = account.Max("_id").AsInt32 + 1;
    //                if (model.money > 0) { model.money = feecharge(model.money); }//fee charge
    //                account.Insert(model);
    //                result = true;
    //            }
    //        }
    //        catch
    //        {
    //        }
    //        finally
    //        {
    //            var tr = dbMng.Transactions(db);
    //            var trans = new TransactionModel()
    //            {
    //                toibans = model.ibans,
    //                amount = model.money,
    //                status = result ? Transactionstatus.Success : Transactionstatus.Failed,
    //                transactiontype = Transactiontype.Firstcreate
    //            };
    //            tr.Insert(trans);
    //        }
    //        return result;
    //    }

    //    public static List<AccountModel> GetAccountData(string[] iban)
    //    {
    //        using (var db = dbMng.Connect())
    //        {
    //            // Get Accounts collection
    //            var account = dbMng.Accounts(db);
    //            return account.Find(x => iban.Contains(x.ibans) && x.status).ToList();
    //        }
    //    }

    //    public static List<AccountModel> GetAccountDataAll()
    //    {

    //        using (var db = dbMng.Connect())
    //        {

    //            // Get Accounts collection
    //            var account = dbMng.Accounts(db);
    //            return account.FindAll().ToList();
    //        }

    //    }

    //    public static List<AccountModel> GetAccountDataBy(string expr)
    //    {

    //        //if (expr.Trim() != "") {
    //        //    using (var db = dbMng.Connect()) {

    //        //        // Get Accounts collection
    //        //        var account = dbMng.Accounts(db);
    //        //        return account.FindAll().ToList();
    //        //    }
    //        //}

    //        return null;
    //    }

    //    #endregion

    //    #region Money

    //    const double feechrgfactor = 0.999; //0.1%
    //    public static bool DepositMoney(TransactionModel trans)
    //    {
    //        var db = dbMng.Connect();
    //        var result = false;
    //        try
    //        {

    //            // Get Accounts collection
    //            var account = dbMng.Accounts(db);
    //            var acc = account.FindOne(a => a.ibans == trans.toibans);

    //            if (acc != null)
    //            {
    //                acc.money += feecharge(trans.amount);
    //                account.Update(acc);

    //                trans.status = Transactionstatus.Success;
    //                result = true;
    //            }

    //        }
    //        catch
    //        {

    //        }
    //        finally
    //        {
    //            var tr = dbMng.Transactions(db);
    //            trans.status = result ? Transactionstatus.Success : Transactionstatus.Failed;
    //            trans.updatedate = DateTime.Now;
    //            tr.Insert(trans);
    //        }

    //        return result;
    //    }

    //    public static bool TransMoney(TransactionModel trans)
    //    {
    //        var db = dbMng.Connect();
    //        var result = false;
    //        try
    //        {

    //            var account = dbMng.Accounts(db);
    //            var fromacc = account.FindOne(a => a.ibans == trans.fromibans);
    //            var toacc = account.FindOne(a => a.ibans == trans.toibans);

    //            try
    //            {
    //                if (fromacc != null && toacc != null &&
    //                        trans.amount < fromacc.money)
    //                {

    //                    //transfer money
    //                    var tmpfromacc = new AccountModel();
    //                    tmpfromacc = fromacc;
    //                    tmpfromacc.money -= trans.amount;

    //                    var tmptoacc = new AccountModel();
    //                    tmptoacc = toacc;
    //                    tmptoacc.money += trans.amount;

    //                    account.Update(tmpfromacc);
    //                    account.Update(tmptoacc);
    //                    result = true;
    //                }
    //            }
    //            catch
    //            {
    //                //roll back
    //                account.Update(fromacc);
    //                account.Update(toacc);
    //            }
    //        }
    //        catch
    //        {

    //        }
    //        finally
    //        {
    //            var tr = dbMng.Transactions(db);
    //            trans.status = result ? Transactionstatus.Success : Transactionstatus.Failed;
    //            trans.updatedate = DateTime.Now;
    //            tr.Insert(trans);
    //        }

    //        return result;
    //    }

    //    #endregion

    //    #region Security

    //    public static void InsertToken(TransactionTokenModel tk)
    //    {
    //        using (var db = dbMng.Connect())
    //        {

    //            var tbtoken = dbMng.Tokens(db);
    //            tbtoken.Insert(tk);
    //        }
    //    }
    //    public static void UpdateToken(TransactionTokenModel tk)
    //    {
    //        using (var db = dbMng.Connect())
    //        {

    //            var tbtoken = dbMng.Tokens(db);
    //            tk.isactive = false;
    //            tk.activatedatetime = DateTime.Now;
    //            tbtoken.Update(tk);
    //        }
    //    }

    //    #endregion

    //    #region ultilities fn.

    //    private static double feecharge(double money)
    //    {
    //        if (money > 0)
    //            return Math.Round((money * feechrgfactor), 2);
    //        else
    //            return money;
    //    }
        #endregion
    //}
}
