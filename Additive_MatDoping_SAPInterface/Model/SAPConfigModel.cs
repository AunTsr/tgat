﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Additive_MatDosing_SAPInterface.Model {
    public class SAPConfigModel {
        public string Location { get; set; }
        public string Backup { get; set; }
        public decimal Cycle { get; set; }
        public Dictionary<string, string> TagsMapping { get; set; } = new Dictionary<string, string>();
    }

}
