﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Additive_MatDosing_SAPInterface.Model {
    public class TankModel {
        public string Start { get; set; }
        public string Done { get; set; }
        public string Ready { get; set; }
        public string PV  { get; set; }
        public string SV { get; set; }
        public string MatID { get; set; }
    }
    //public class TankModelTAG
    //{
    //    public string Start { get; set; }
    //    public string Done { get; set; }
    //    public string Ready { get; set; }
    //    public string PV { get; set; }
    //    public string SV { get; set; }
    //    public string MatID { get; set; }

    //    public string TagName { get; set; }
    //}

    public class TankModelX
    {
        public Double Target { get; set; }
        public Double Current { get; set; }
        public Double Remain { get; set; }
        public TankStatus Status { get; set; }
        public string MatTag { get; set; }
    }

    public enum TankStatus {
        NotDefine = 0,
        ProcessStart = 1,
        ScanedReadyToFill = 2,
        Dosing = 3,
        FinishDosing= 4,
        Waiting = 5,
        Dropping = 6,
        InitialStatus = 7,
        Complete = 8,
        Mannual = 9,
        ScadaConfirmed = 10,
        Cancel = 11,
        IsReady = 12
    }

    public enum ErrorStatus
    {
        Normal = 0,
        MissMaterial = 1,
        InnerlockDoor = 2,
        ErrorPVLow = 3,
        CheckInitial = 4
    }
}
