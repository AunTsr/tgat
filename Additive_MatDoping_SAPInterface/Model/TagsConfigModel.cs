﻿using S7.Net;
using S7NetWrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Additive_MatDosing_SAPInterface.Model {
    public class TagsConfigModel {
        public string IpAddress { get; set; }
        public string Cputype { get; set; }
        public short Rack { get; set; }
        public short Slot { get; set; }
        public decimal Cycle { get; set; }
        public List<TagsMappingMoldel> Tags { get; set; } = new List<TagsMappingMoldel>();
    }

    public class TagsMappingMoldel {
        public string SystemParameter { get; set; }
        public Tag PLCTagsAddress { get; set; }
        public string ReadWrite { get; set; }
    }

    public class TagsConfigModelX
    {
        public string IpAddress { get; set; }
        public string Cputype { get; set; }
        public short Rack { get; set; }
        public short Slot { get; set; }
        public decimal Cycle { get; set; }
        public List<TagsMappingMoldelX> Tags { get; set; } = new List<TagsMappingMoldelX>();
    }

    public class TagsMappingMoldelX
    {
        public string SystemParameter { get; set; }
        public Tag PLCTagsAddress { get; set; }
        public string DataType { get; set; }
        public string ReadWrite { get; set; }
    }
}
