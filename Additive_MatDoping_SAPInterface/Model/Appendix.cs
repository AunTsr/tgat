﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Additive_MatDosing_SAPInterface.Model
{
    public class Appendix
    {
        public static Dictionary<string, string> Dic = new Dictionary<string, string>();

        public static void Plus(string XmlTags, string SystemVars)
        {
            Dic.Add(XmlTags, SystemVars);
        }

        public static void Plus(Dictionary<string,string> pairsDic)
        {
            Dic = pairsDic;
        }

        public static string getVal(string key)
        {
            return Dic[key];
        }
    }
}
