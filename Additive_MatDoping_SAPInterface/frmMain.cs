﻿using Additive_MatDosing_SAPInterface.services;
using Additive_MatDosing_SAPInterface.UI;
using System;
using System.Windows.Forms;

namespace Additive_MatDosing_SAPInterface {
    public partial class frmMain : Form {

        enum page {
            home,
            sapConf,
            tags,
            formula
        }
        public frmMain() {
            InitializeComponent();
            Loadpage(page.home);
            toolStripTextBox1.Text = Properties.Settings.Default.NumPO.ToString();
        }

        private void serviceToolStripMenuItem_Click(object sender, EventArgs e) {
            Loadpage(page.home);
        }

        private void formularToolStripMenuItem_Click(object sender, EventArgs e) {
            Loadpage(page.formula);
        }

        private void sAPConfigToolStripMenuItem_Click(object sender, EventArgs e) {
            Loadpage(page.sapConf);
        }

        private void tagstoolStripMenuItem_Click(object sender, EventArgs e) {
            Loadpage(page.tags);
        }

        private void Loadpage(page p) {
            this.panel1.Controls.Clear();
            switch (p) {
                case page.home:
                    this.panel1.Controls.Add(new ucHome());
                    break;
                case page.sapConf:
                    this.panel1.Controls.Add(new ucSAP_conf());
                    break;
                case page.tags:
                    this.panel1.Controls.Add(new ucTags());
                    break;
                case page.formula:
                    this.panel1.Controls.Add(new ucformula());
                    break;
            }
            this.panel1.Controls[0].Dock = DockStyle.Fill;
        }

        private void TestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GenXML2Sap a = new GenXML2Sap();
            a.StempDone("C:\\Users\\prata\\Desktop\\TestSap.XML");
            new ForceVal().Show();
        }

        string LastData;
        private void ToolStripTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                Properties.Settings.Default.NumPO = toolStripTextBox1.Text._2int();
                Properties.Settings.Default.Save();
                MessageBox.Show("Save!");
            }
        }

        private void FrmMain_Leave(object sender, EventArgs e)
        {
            Application.ExitThread();
            Global.SystemSvc.ProgramRun = false;
        }
    }
}
