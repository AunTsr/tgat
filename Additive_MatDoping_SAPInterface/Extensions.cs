﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Additive_MatDosing_SAPInterface {
    public static class DgRowExtentions {
        public static string _StrVal(this DataGridViewRow r,int index) {
            try {
                return r.Cells[index].Value.ToString();
            }
            catch{
                return null;
            }
        }

        public static string _StrVal(this DataRow r, string colname)
        {
            try
            {
                return r[colname].ToString();
            }
            catch
            {
                return null;
            }
        }
    }

    public static class ConvertExtentions {
        public static int _2int(this object val) {
            try {
                return Convert.ToInt32(val);
            }
            catch {
                return default(int);
            }
        }
        public static double _2db(this object val) {
            try {
                return Convert.ToDouble(val);
            }
            catch {
                return default(double);
            }
        }
    }
}
