﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Additive_MatDosing_SAPInterface.services
{
    public class GenXML2Sap
    {
        public void StempDone(string FilePath)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(FilePath);
            XmlNode Done = xmlDoc.CreateNode(XmlNodeType.Element,"Done",null);
            Done.InnerText = "True";
            xmlDoc.DocumentElement.AppendChild(Done);
            xmlDoc.Save(FilePath);
        }       
    }
}
