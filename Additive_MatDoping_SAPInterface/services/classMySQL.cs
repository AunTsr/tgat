﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using NUnit;
using NUnit.Framework;

namespace ExMYsql
{
    public class ClassMySQL
    {
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Con { get; set; }
        MySqlConnection mySql;

        public ClassMySQL()
        {

        }

        public ClassMySQL(string Connnect)
        {
            this.Con = Connnect;
        }

        public ClassMySQL(string Server, string Database, string User, string Password)
        {
            this.ServerName = Server;
            this.DatabaseName = Database;
            this.UserName = User;
            this.Password = Password;
            Con = $"Server={Server};Database={Database};Uid={User};Pwd={Password};";
        }

        public bool Connect()
        {
            try
            {
                mySql = new MySqlConnection(Con);
                mySql.Open();
                mySql.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DisConnect()
        {
            try
            {
                mySql.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public DataTable CallStore(string Store,int max, List<string> Params)//----------------------------------------------------------------------------
        {
            //Params.AddRange(new string[] { "A06"});
            string Cmd = $"call {Store} ({max} ,"; 

            for (int i = 0; i < 20; i++)
            {
                if(Params.Count-1 >= i) Cmd = String.Concat(Cmd,"\'" + Params[i] + "\'" + ",");
                else Cmd = String.Concat(Cmd, "\'" + "" + "\'" + ",");
            }
            Cmd = Cmd.Substring(0, Cmd.Length-1)+");";


            //var pars = "'" + String.Join("','", Params) + "'";
            ////var aaa = "select * from productionorder pd"
            ////          + " join order_material mat on mat.production_order = pd.production_order"
            ////          + " where pd.status = 'wait' and pd.production_order not in (            "
            ////          + " select distinct production_order from order_material                 "
            ////          + $" where material in ({pars}) and status = 'wait')                   ";

            //var sql = $"select ID, pd2.Production_Order,Material,mat.Values,status from"          
            //         +" (select production_order from productionorder pd "
            //         +" where pd.status = 'wait'"
            //         +" and pd.production_order not in" 
            //         +"(select distinct production_order from order_material"
            //         + $" where material in ({pars}) and status = ('wait') "
            //         + $" limit {max} pd2 "
            //         +" join order_material mat on mat.production_order = pd2.production_order   ";               

            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataAdapter Val = new MySqlDataAdapter(Cmd, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return new DataTable(); ;
            }
        }

        public void CallStore(string Store)
        {
            string Cmd = $"call {Store};";
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
            }
            catch
            {

            }
        }

        public DataTable CallStores(string Store)
        {
            string Cmd = $"call {Store};";
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataAdapter Val = new MySqlDataAdapter(Cmd, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return new DataTable();
            }
        }

        public DataTable CallStores(string Store, int max)
        {
            string Cmd = $"call {Store}({max});";
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataAdapter Val = new MySqlDataAdapter(Cmd, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return null;
            }
        }

        public DataTable SelectDataAll(string Table)
        {
            string Cmd = $"Select * from {Table};";
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataAdapter Val = new MySqlDataAdapter(Cmd, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return null;
            }
        }

        public DataTable SelectData(string Query)
        {
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Query, mySql);
                MySqlDataAdapter Val = new MySqlDataAdapter(Query, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return new DataTable(); 
            }
        }

        public DataTable SelectData(string Table, List<string> ColName)//=========================================================================
        {
            string CmdBegin = "Select";
            string CmdEnd = $"From {Table}";
            for(int i= 0; i < ColName.Count; i++)
            {
                CmdBegin = String.Concat(CmdBegin, ColName[i]+",");
            }
            string Cmd = CmdBegin.Substring(0, CmdBegin.Length-1) + CmdEnd+";";
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataAdapter Val = new MySqlDataAdapter(Cmd, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return null;
            }
        }

        public bool InsertData(string Table, List<string> ColName, List<string> Value)
        {
            string CmdBegin = $"Insert Into {Table} (";
            string CmdEnd = $"Values (";
            for (int i = 0; i < ColName.Count; i++)
            {
                CmdBegin = String.Concat(CmdBegin,ColName[i].ToString() + ",");
                CmdEnd = String.Concat(CmdEnd, "\'" + Value[i].ToString() + "\'" + ",");
            }
            string Cmd = CmdBegin.Substring(0, CmdBegin.Length - 1)+")" + CmdEnd.Substring(0, CmdEnd.Length-1)+")" + ";";
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool InsertData(string Table, string Source, string Datails)
        {
            string Cmd = $"INSERT INTO {Table} (`Source`,`Details`,`Time`) VALUES ('{Source}','{Datails}','{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}'); ";
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateData(string Table, List<string> ColName, List<string> Value, string Where)//================================================
        {
            string CmdBegin = $"UPDATE  {Table} SET (";
            string CmdEnd = $"WHERE {Where}";
            for (int i = 0; i < ColName.Count; i++)
            {
                CmdBegin = String.Concat(CmdBegin, "`" +ColName[i]+"`" +"="+ "\""+ Value[i]+ "\"");
            }
            string Cmd = CmdBegin.Substring(0, CmdBegin.Length - 1) + ")" + CmdEnd +";";
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateData(string Table, string ColName, string Value, string Where)//================================================
        {
            string Cmd = $"Update {Table} SET {ColName} = '{Value}' Where {Where}";
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteData(string Table, string Where)
        {
            string Cmd = $"DELETE FROM {Table} WHERE {Where};";
            try
            {
                mySql.Open();
                MySqlCommand Command = new MySqlCommand(Cmd, mySql);
                MySqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
