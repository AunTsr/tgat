﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Additive_MatDosing_SAPInterface.Model;

namespace Additive_MatDosing_SAPInterface
{
    public static class XmlManage
    {
        public static string PathFile { get; set; }
        public static string PathFileBK { get; set; }
        public static string[] fileNames;
        public static string TagName = "";
        public static int range { get; set; }

        public static Dictionary<string, string> xmldata = new Dictionary<string, string>();
        public static List<Dictionary<string, string>> listxmldata = new List<Dictionary<string, string>>();

        //public  XmlManage()
        //{

        //}

        //public  XmlManage(string PathFile, int range)
        //{
        //    this.PathFile = PathFile;
        //    this.range = range;
        //}

        public static List<Dictionary<string, string>> GetData()
        {
            fileNames = Directory.GetFiles(PathFile, "*.XML");

            //read all file 
            foreach (string file in fileNames)
            {
                // Read a document  
                XmlTextReader textReader = new XmlTextReader(file);
                try
                {
                //XmlTextReader textReader = new XmlTextReader(fileNames[0]);

                    int i = 0;
                    int j = 0;

                // Read until end of file  
                while (textReader.Read())
                {
                    XmlNodeType nType = textReader.NodeType;

                    if (textReader.Value.ToString() != "")
                    {
                        if (i == 1)
                        {
                            try
                            {
                                xmldata.Add(Appendix.getVal(TagName), textReader.Value.ToString());
                                i = 0;
                                j++;
                                TagName = "";
                            }
                            catch
                            {

                            }
                        }
                    }

                    // if node type is an element  
                    if (nType == XmlNodeType.Element)
                    {
                        if (textReader.Name.ToString() == textReader.Name.ToString())
                        {
                            i = 1;
                            TagName = textReader.Name.ToString();
                        }
                    }

                    if (j == range)
                    {
                        listxmldata.Add(xmldata);
                        j = 0;
                        xmldata = new Dictionary<string, string>();
                    }
                }
                textReader.Close();

                string filename = Path.GetFileName(file);
                var fullpath = PathFileBK + "\\" + filename;

                if (File.Exists(fullpath)){
                    File.Delete(fullpath);
                }
                File.Move(file, fullpath);
                }
                catch
                {

                }
            }
            return listxmldata;
        }
    }
}
