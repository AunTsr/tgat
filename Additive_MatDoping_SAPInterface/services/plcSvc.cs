﻿using Additive_MatDosing_SAPInterface.Model;
using Additive_MatDosing_SAPInterface.PlcConnectivity;
using S7NetWrapper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace Additive_MatDosing_SAPInterface.services {
    public class plcSvc {
        public static bool IsReady ;
        static TagsConfigModel config;
        public static Dictionary<string, Tag> PlcTagsRead = new Dictionary<string, Tag>();
        public static Dictionary<string, Tag> PlcTagsWrite = new Dictionary<string, Tag>();
        public static Dictionary<string, int> Mat2Tank = new Dictionary<string, int>();
        //public static List<TankModelTAG> TankList = new List<TankModelTAG>();

        public static void LoadConfig() {
            config = Helper.JsontoObj<TagsConfigModel>(
                            Helper.ReadTxtfile(Helper.TagsConfig_)
                       );

            PlcTagsRead.Clear();
            PlcTagsWrite.Clear();
            foreach (var t in config.Tags) {
                //PlcTagsAll.Add(t.SystemParameter, t.PLCTagsAddress);
                if (t.ReadWrite == "Read") {
                    if (!PlcTagsRead.ContainsKey(t.SystemParameter)) {
                        PlcTagsRead.Add(t.SystemParameter, t.PLCTagsAddress);
                    }
                    else {
                        PlcTagsRead[t.SystemParameter] = t.PLCTagsAddress;
                    }
                }
                else {
                    if (!PlcTagsWrite.ContainsKey(t.SystemParameter)) {
                        PlcTagsWrite.Add(t.SystemParameter, t.PLCTagsAddress);
                    }
                    else {
                        PlcTagsWrite[t.SystemParameter] = t.PLCTagsAddress;
                    }
                }
            }
            IsReady = true;
            Connect();
        }

        public static bool Connect() {
            try {
                if (!IsReady) LoadConfig();
                PlcS7.Instance.Connect(config.IpAddress, config.Cputype,config.Rack,config.Slot);
                IsReady = true;
                return true;
            }
            catch (Exception exc) {
                MessageBox.Show(exc.Message);
                IsReady = false;
                return false;
            }
        }
        public static bool Connect(string ip, string cputype, short rack, short slot) {
            try {
                //if (!IsReady) LoadConfig();//-------------------------------------------------------------------------add
                PlcS7.Instance.Connect(ip, cputype, rack, slot);
                if (PlcS7.Instance.ConnectionState == ConnectionStates.Online)
                    IsReady = true;
                else IsReady = false;
                return IsReady;
            }
            catch (Exception exc) {
                MessageBox.Show(exc.Message);
                IsReady = false;
                return false;
            }
        }

        public static void Disconnect() {
            try {
                PlcS7.Instance.Disconnect();
                IsReady = false;
            }
            catch (Exception exc) {
                MessageBox.Show(exc.Message);
            }
        }

        public static void Write(string tags, object val) {
            try {
                if(IsReady) PlcS7.Instance.Write(tags, val);
            }
            catch {
                IsReady = false;
            }
        }
        public static object Read(string tag) {
            try {
                return PlcS7.Instance.Read(tag);
            }
            catch {
                IsReady = false;
                return null;
            }
        }

        #region process
        public static bool WriteParameters(List<Tag> tags) {
            try {
                if (IsReady) PlcS7.Instance.Write(tags);
                return true;
            }
            catch {
                return false;
            }
        }

        public static List<Tag> ReadTags(List<Tag> tags) {
            try {
                return PlcS7.Instance.Read(tags);
            }
            catch {
                return null;
            }
        }
        public static Dictionary<string, object> ReadParameters() {
            try {
                var readresult = PlcS7.Instance.Read(PlcTagsRead.Values.ToList()).ToDictionary(x=>x.ItemName, x => x.ItemValue??default);
                //foreach(Tag item in PlcS7.Instance.Read(PlcTagsRead.Values.ToList()))
                //{
                //    readresult.Add(item.ItemName, item.ItemValue);
                //    Debug.Print(item.ItemName);
                //}
                var result = new Dictionary<string, object>();
                foreach (var t in PlcTagsRead)
                {
                    result.Add(t.Key, readresult[t.Value.ItemName]);
                }
                    #region note
                    //    {
                    //        case "Start":
                    //            TankList[tNum].Start = t.Value.ItemName.ToString();
                    //            break;
                    //        case "Done":
                    //            TankList[tNum].Done = t.Value.ItemName.ToString();
                    //            break;
                    //        case "Ready":
                    //            TankList[tNum].Ready = t.Value.ItemName.ToString();
                    //            break;
                    //        case "PV_Mat":
                    //            TankList[tNum].PV = t.Value.ItemName.ToString();
                    //            break;
                    //        case "SV_Mat":
                    //            TankList[tNum].SV = t.Value.ItemName.ToString();
                    //            break;
                    //        case "MatID":
                    //            TankList[tNum].MatID = t.Value.ItemValue.ToString().Replace("\0", string.Empty);
                    //            break;
                    //    }
                    //    //TankList[tNum].TagName = t.Key;
                    //    }
                    //foreach (var dt in TankList)
                    //{
                    //    PlcTagsAll.Add(dt.MatID, dt);
                    //}
                    #endregion
                    return result;
            }
            catch(Exception ex) {
                //MessageBox.Show(ex.Message+"\r\nPLC is not ready");
                return null;
            }
        }

        public static void SetTankMat()
        {
            Mat2Tank.Clear();
            try
            {
                foreach (var t in PlcTagsRead)
                {
                    var par = t.Key.Split('#');
                    var tNum = par[1]._2int();
                    switch (par[0])
                    {
                        case "MatTag":
                            Mat2Tank.Add(t.Value.ItemValue.ToString().Replace("\0", string.Empty), tNum-1);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Invalid to create PLC material name [plcSvc.SetTankMat]");
                //PLC lost connection;
            }
        }
        #endregion
    }
}
