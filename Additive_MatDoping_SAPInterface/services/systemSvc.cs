﻿using Additive_MatDosing_SAPInterface.Model;
using Additive_MatDosing_SAPInterface.PlcConnectivity;
using Additive_MatDosing_SAPInterface.UI;
using ExMYsql;
using S7.Net;
using S7NetWrapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Additive_MatDosing_SAPInterface.services {
    public class systemSvc {

        #region props
        public List<TankModelX> TankList { get; set; } = new List<TankModelX>();
        List<string> InProcessMaterial = new List<string>();
        public Queue<QueueModel> QList = new Queue<QueueModel>();
        private List<string> IsCancel = new List<string>();
        public bool Logging = false;
        bool ThrdStop = true;
        public bool isrunning;
        public DataTable SourceTable = new DataTable("DataSource");
        public int OrderCnt = 0;
        string ConStr = Helper.ReadTxtfile(AppDomain.CurrentDomain.BaseDirectory + "DB_Set.Con");
        public List<string> InMonitorDelete = new List<string>();
        public DataTable TableA = new DataTable();
        public DataTable TableB = new DataTable();
        public DataTable TableC = new DataTable();

        public List<string> lstMsg = new List<string>();
        public List<Color> lstColor = new List<Color>();
        public string NowMsg;
        public bool ProgramRun = true;
        public List<string> ArrMsg = new List<string>();
        private Array temp = Array.CreateInstance(typeof(string),6);
        bool IsReset = false;
        bool IsSendCommand = false;


        // public int count = Properties.Settings.Default.Count;

        #endregion

        #region events

        public event EventHandler<EventArgs> ServiceStatusChange;
        protected void OnServiceStatusChange(EventArgs e) {
            EventHandler<EventArgs> handler = ServiceStatusChange;
            if (handler != null) {
                handler(this, e);
            }
        }


        public event EventHandler<EventArgs> bgStarted;
        protected void OnbgStarted(EventArgs e) {
            EventHandler<EventArgs> handler = bgStarted;
            if (handler != null) {
                handler(this, e);
            }
        }

        public event EventHandler<EventArgs> bgStopped;
        protected void OnbgStopped(EventArgs e) {
            EventHandler<EventArgs> handler = bgStopped;
            if (handler != null) {
                handler(this, e);
            }
        }

        public event EventHandler<EventArgs> ReadComplete;
        protected void OnReadComplete(EventArgs e) {
            EventHandler<EventArgs> handler = ReadComplete;
            if (handler != null) {
                handler(this, e);
            }
        }

        public event EventHandler<TankValueChangeArg> Datachange;
        protected void OnDatachange(TankValueChangeArg e)
        {
            EventHandler<TankValueChangeArg> handler = Datachange;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<DataSourceChangeArg> DataSourceChange;
        protected void OnDataSourceChange(DataSourceChangeArg e)
        {
            EventHandler<DataSourceChangeArg> handler = DataSourceChange;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<MaterialProcessingDataChangeArg> MaterialProcessingDataChange;
        protected void OnMaterialProcessingDataChange(MaterialProcessingDataChangeArg e)
        {
            EventHandler<MaterialProcessingDataChangeArg> handler = MaterialProcessingDataChange;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<SystemMsgArg> SystemMsg;
        protected void OnSystemMsg(SystemMsgArg e)
        {
            EventHandler<SystemMsgArg> handler = SystemMsg;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<SystemTableArg> TableMange;
        protected void OnDataSourceRow(SystemTableArg e)
        {
            EventHandler<SystemTableArg> handler = TableMange;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<SystemLostArg> LostConnect;
        protected void OnLostConnct(SystemLostArg e)
        {
            EventHandler<SystemLostArg> handler = LostConnect;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        //public event EventHandler<DeleteRowChangeArg> DeleteRowChange;
        //protected void OnDeleteRowChange(DeleteRowChangeArg e)
        //{
        //    EventHandler<DeleteRowChangeArg> handler = DeleteRowChange;
        //    if (handler != null)
        //    {
        //        handler(this, e);
        //    }
        //}

        #endregion

        #region Init
        public systemSvc() {
            Init(20);
            SetArr();
        }

        private void Init(int tcnt) {
            TankList.Clear();
            for (var i = 0; i < tcnt; i++) {
                TankList.Add(new TankModelX());
            }
        }

        #endregion

        #region Processing

        public bool IsRunning {
            get => isrunning;
            set {
                if (isrunning != value) {
                    isrunning = value;
                    OnServiceStatusChange(null);
                }
            }
        }

        public void Start() {
            if (!IsRunning && ThrdStop) {
                IsRunning = true;
                new Task(() => bgservice()).Start();
            }
            else if (!ThrdStop) {
                MessageBox.Show("Thread still running!, please wait...");
            }
            else {
                IsRunning = false;
            }
        }
        public void Stop() {
            if(!IsSendCommand)
            {
                IsRunning = false;
                IsReset = true;
                if (TableC.Columns.Count != 0 || TableC.Rows.Count != 0)
                {
                    TableC.Columns.Clear();
                    TableC.Rows.Clear();
                }
                TableC = SourceTable.Copy();
            }
            else
            {
                MessageBox.Show("Command have sended to PLC");
            }

        }

        private void SetArr()
        {
            temp.SetValue("None", 0);
            temp.SetValue("None", 1);
            temp.SetValue("None", 2);
            temp.SetValue("None", 3);
            temp.SetValue("None", 4);
            temp.SetValue("None", 5);

            SourceTable.Columns.Clear();
            SourceTable.Rows.Clear();
            SourceTable.Columns.Add("OrderNo", typeof(int));
            SourceTable.Columns.Add("MaterialNo", typeof(string));
            SourceTable.Columns.Add("Target", typeof(string));
            SourceTable.Columns.Add("Current", typeof(string));
        }

        public void bgservice() {

            ThrdStop = false;

            if (IsReset)
            {
                SourceTable.Columns.Clear();
                SourceTable.Rows.Clear();
                SourceTable = TableC.Copy();
            }

            OnDataSourceChange(new DataSourceChangeArg(SourceTable));
            // get Data From Database
            // In The First Time

            OnbgStarted(null);
            GetdataInfirst("OrderFirst");
            CheckDataProcess();
            new Thread(() => DataRead()).Start();
            new Thread(() => CheckQ()).Start();
            IsReset = false;

            while (IsRunning)
            {
                //to do -> get source file from SAP
                //if have source file then extract value
                getDataFromSAP2DB();

                // In Process (Update model status "Process","Done" 
                //And database productionorder_command status "Process","Done")

                if (Logging)
                {
                    File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "\\" + "LoggingData\\" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + ".txt", "\r\n" + string.Join("\r\n", this.lstMsg.ToArray()));
                }

                this.OrderCnt = SourceTable.AsEnumerable().Select
                    (row => new {
                         Order = row.Field<int>("OrderNo")
                    }).Distinct().Count();


                if (Properties.Settings.Default.NumPO > this.OrderCnt)
                {
                    Getdata("OrderQuery", 1);
                }

                //OnLostConnct(new SystemLostArg());
                //OnDataSourceChange(new DataSourceChangeArg(SourceTable));
                UpdateToDatagridView();
                Thread.Sleep(1000);
            }
            ThrdStop = true;
            OnbgStopped(null);
        }

        void DataRead()
        {
            while(ProgramRun && IsRunning)
            {
                readvals();
                Application.DoEvents();
                Thread.Sleep(1000);
            }
        }

        void getDataFromSAP2DB()
        {
            var ProductionOrder = new List<Dictionary<string, string>>();
            var Col = new List<string>();
            var Val = new List<string>();

            var ColOrder = new List<string>();
            var ValOrder = new List<string>();

            var PO_check = new List<string>();

            ucSAP_conf SetParams = new ucSAP_conf();
            SetParams.Dispose();

            ProductionOrder = XmlManage.GetData();

            if(ProductionOrder.Count != 0)
            {
                ClassMySQL ToDb = new ClassMySQL(ConStr);
                ToDb.Connect();
                try
                {
                    foreach (Dictionary<string, string> Dic in ProductionOrder)
                    {
                        //ColOrder.Add("ID");
                        //ValOrder.Add(count.ToString());
                        //Col.Add("ID");
                        //Val.Add(count.ToString());

                        if (!PO_check.Contains(Dic["OrderNo"].ToString()))
                        {
                            PO_check.Add(Dic["OrderNo"].ToString());
                            foreach (KeyValuePair<string, string> item in Dic)
                            {
                                if(item.Key.ToString() == "OrderNo")
                                {
                                    Col.Add(item.Key);
                                    Val.Add(item.Value);
                                }
                            }
                            Col.Add("Status");
                            Val.Add("Wait");
                            Col.Add("GettingTime");
                            Val.Add(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            ToDb.InsertData("production_order", Col, Val);
                            //ToDb.CallStore("ExtractData");
                            Col.Clear();
                            Val.Clear();
                        }

                        foreach (KeyValuePair<string, string> item in Dic)
                        {
                            if (item.Key.ToString() == "OrderNo" || item.Key.ToString() == "MJALotNo" || item.Key.ToString() == "MaterialNo" || item.Key.ToString() == "MaterialLotNo" || item.Key.ToString() == "Quantity")
                            {
                                ColOrder.Add(item.Key);
                                ValOrder.Add(item.Value);
                            }
                        }

                        ColOrder.Add("Status");
                        ValOrder.Add("Wait");
                        ToDb.InsertData("production_order_material", ColOrder, ValOrder);
                        ColOrder.Clear();
                        ValOrder.Clear();
                        //count++;
                        //Properties.Settings.Default.Count = count;
                        //Properties.Settings.Default.Save();
                    }

                    //foreach (Dictionary<string, string> Dic in ProductionOrder)
                    //{
                    //    foreach (KeyValuePair<string, string> item in Dic)
                    //    {
                    //        if (item.Key.ToString() != "RawMaterialNo" || item.Key.ToString() != "Quantity" || item.Key.ToString() != "Note")
                    //        {
                    //            ColOrder.Add(item.Key);
                    //            ValOrder.Add(item.Value);
                    //        }
                    //    }
                    //    ColOrder.Add("Status");
                    //    ValOrder.Add("Wait");
                    //    ColOrder.Add("FromSap");
                    //    ValOrder.Add(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    //    ToDb.InsertData("production_order_material", Col, Val);
                    //    ColOrder.Clear();
                    //    ValOrder.Clear();
                    //}

                    if(temp.GetValue(0).ToString()!="None")
                    {
                        temp.SetValue("None",0);
                    }
                    ProductionOrder.Clear();
                    ToDb.DisConnect();
                }
                catch (Exception ex)
                {
                    // cannot read xml or declear xml data to database
                    if(temp.GetValue(0).ToString() == "None")
                    {
                        temp.SetValue($"{ex.Message}[getDataFromSAP2DB]",0);
                        OnSystemMsg(new SystemMsgArg($"{ex.Message}[getDataFromSAP2DB]", Color.Red));
                        SendError("System.getDataFromSAP2DB", $"{ex.Message}[getDataFromSAP2DB]");
                    }
                    //OnSystemMsg(new SystemMsgArg(ex.ToString()));
                }
            }
        }

        public void InitPLCTagmodel()
        {
            plcSvc.LoadConfig();
        }

        private void CheckQ()
        {
            while(IsRunning && ProgramRun)
            {
                if (QList.Count != 0)
                {
                    var qq = new QueueModel();
                    qq = QList.Dequeue();
                    if((TankStatus)TankList[qq.tank].Status != TankStatus.IsReady)
                    {
                        QList.Enqueue(qq);
                    }
                    else
                    {
                        sendDosingCmd(qq.ID, qq.tank, qq.Value, qq.ProductionOrder, qq.Mat);
                    }
                    //while ((TankStatus)TankList[qq.tank].Status != TankStatus.IsReady)
                    //{
                    //    Thread.Sleep(1000);
                    //    Application.DoEvents();
                    //}
                    //sendDosingCmd(qq.ID, qq.tank, qq.Value, qq.ProductionOrder, qq.Mat);
                }
                Thread.Sleep(3000);
                Application.DoEvents();
            }
        }

        private void Getdata(string Store, int Max)
        {
            ClassMySQL mySQL = new ClassMySQL(ConStr);
            mySQL.Connect();
            DataTable Data = new DataTable();
            Data = mySQL.CallStore(Store, Max, InProcessMaterial);

            //this.OrderCnt = this.OrderCnt + Data.AsEnumerable()
            //        .Select(row => new
            //        {
            //            Order = row.Field<string>("OrderNo")
            //        }).Distinct().Count();

            string IDData = "";
            //List<string>  DtCount = new List<string>();

            if (Data.Rows.Count != 0)
            {
                IsSendCommand = true;
                foreach (DataRow dr in Data.Rows)
                {
                    try
                    {
                        mySQL.UpdateData("production_order_material", "TankNo", $"{(Convert.ToInt32(plcSvc.Mat2Tank[dr["MaterialNo"].ToString()]) + 1).ToString()}", $"production_order_material.OrderNo = {dr["OrderNo"]} and production_order_material.MaterialNo = '{dr["MaterialNo"]}'");
                        if (temp.GetValue(1).ToString() != "None") 
                        {
                            temp.SetValue("None", 1);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (temp.GetValue(1).ToString() == "None")
                        {
                            temp.SetValue($"{ex.Message}[Getdata] >>> Update tank to database", 1);
                            OnSystemMsg(new SystemMsgArg($"{ex.Message}[Getdata] >>> Update tank to database", Color.Red));
                            SendError(dr["MaterialNo"].ToString(), $"{ex.Message}[Getdata] >>> Update tank to database");
                        }
                    }

                    if (IDData != dr["ID"].ToString())
                    {
                        IDData = dr["ID"].ToString();
                    }
                    else if (IsCancel.Contains(dr["ID"].ToString()))
                    {
                        try
                        {
                            IsCancel.Remove(dr["ID"].ToString());
                            break;
                        }
                        catch
                        {
                            break;
                        }
                    }                  

                    try
                    {                      
                        if ((TankStatus)TankList[plcSvc.Mat2Tank[dr["MaterialNo"].ToString()]].Status == TankStatus.IsReady)
                        {
                            sendDosingCmd(
                                dr._StrVal("ID")
                                , plcSvc.Mat2Tank[dr._StrVal("MaterialNo")]
                                , dr["Quantity"].ToString()
                                , dr["OrderNo"].ToString()
                                , dr["MaterialNo"].ToString()
                                );
                           
                        }
                        else
                        {
                            QueueModel Q = new QueueModel();
                            Q.ID = dr._StrVal("ID");
                            Q.tank = plcSvc.Mat2Tank[dr._StrVal("MaterialNo")];
                            Q.Value = dr["Quantity"].ToString();
                            Q.ProductionOrder = dr["OrderNo"].ToString();
                            Q.Mat = dr["MaterialNo"].ToString();
                            QList.Enqueue(Q);
                            //mySQL.UpdateData("production_order_material", "Status", "Reject", $"production_order_material.ID = '{dr._StrVal("ID")}' and production_order_material.Material = '{dr["Material"].ToString()}'");
                        }

                        //if (!DtCount.Contains(dr["OrderNo"].ToString()))
                        //{
                        //    DtCount.Add(dr["OrderNo"].ToString());
                        //    //this.OrderCnt++;
                        //}

                        if (temp.GetValue(2).ToString() != "None")
                        {
                            temp.SetValue("None", 2);
                        }
                    }
                    catch(Exception ex)
                    {
                        //Dictionary mat2tank is null
                        if (temp.GetValue(2).ToString() == "None")
                        {
                            temp.SetValue($"{ex.Message}[Getdata] >>> To send command", 2);
                            OnSystemMsg(new SystemMsgArg($"{ex.Message}[Getdata] >>> To send command", Color.Red));
                            SendError(dr["MaterialNo"].ToString(), $"{ex.Message}[Getdata] >>> To send command");
                            //OnSystemMsg(new SystemMsgArg(ex.ToString()));
                        }
                    }
                }
            }
            mySQL.DisConnect();
            IsSendCommand = false;
        }

        private void GetdataInfirst(string Store)
        {
            ClassMySQL mySQL = new ClassMySQL(ConStr);
            mySQL.Connect();
            DataTable Data = new DataTable();
            Data = mySQL.CallStores(Store);
            if (Data.Rows.Count == 0) return;
            mySQL.DisConnect();

            //List<string> DtCount = new List<string>();

            #region Note
            //this.OrderCnt = this.OrderCnt + Data.AsEnumerable()
            //    .Select(row => new
            //    {
            //        Order = row.Field<string>("OrderNo")
            //    }).Distinct().Count();

            //var dID = Data.AsEnumerable()
            //    .Select(row => new
            //    {
            //        OrderNo = row.Field<string>("OrderNo")
            //    }).Distinct().ToList();
            //var dMat = Data.AsEnumerable()
            //    .Select(row => new
            //    {
            //        Order = row.Field<string>("MaterialNo")
            //    }).Distinct().ToList();

            //int ctt = dMat.Count;

            //for (int k=0; k < ctt; k++)
            //{
            //    int zz = Convert.ToInt32(dID[k].OrderNo);
            //    if (k < ctt)
            //        //new Thread(() => CkeckInprocess(zz, dMat[k].Order)).Start();
            //        CkeckInprocess(zz, dMat[k].Order);
            //}
            #endregion

            if (Data.Rows.Count != 0)
            {
                foreach (DataRow dr in Data.Rows)
                {
                    try
                    {
                        if(dr["Status"].ToString()=="Inprocess")
                        {
                            if (TankList[plcSvc.Mat2Tank[dr["MaterialNo"].ToString()]].Status != TankStatus.IsReady)
                            {
                                new Thread(() => MonitoringThread(
                                dr["ID"].ToString()
                                , plcSvc.Mat2Tank[dr["MaterialNo"].ToString()]
                                , dr["Quantity"].ToString()
                                , dr["OrderNo"].ToString()
                                , dr["MaterialNo"].ToString()
                                )).Start();

                                InProcessMaterial.Add(dr["MaterialNo"].ToString());

                                //sendDosingCmd(dataRow["ID"].ToString(), dataRow["MaterialNo"].ToString(), dataRow["Values"].ToString(), dataRow["Status"].ToString());
                                if(!IsReset)
                                {
                                    SourceTable.Rows.Add(new object[] { dr["OrderNo"],
                                                        dr["MaterialNo"].ToString(),
                                                        dr["Quantity"].ToString(),
                                                        //dr["Status"].ToString()
                                                        " "
                                    });
                                }
                            }
                            else
                            {
                                mySQL.UpdateData("production_order_material", "Status", "Done", $"production_order_material.OrderNo = {dr["OrderNo"].ToString()} and production_order_material.MaterialNo = '{dr["MaterialNo"].ToString()}';");
                                mySQL.UpdateData("production_order_material", "LastUpdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), $"production_order_material.MaterialNo = '{dr["MaterialNo"].ToString()}' and production_order_material.OrderNo = {dr["OrderNo"].ToString()};");
                                //QueueModel Q = new QueueModel();
                                //Q.ID = dr._StrVal("ID");
                                //Q.tank = plcSvc.Mat2Tank[dr._StrVal("Material")];
                                //Q.Value = dr["Values"].ToString();
                                //Q.ProductionOrder = dr["OrderNo"].ToString();
                                //Q.Mat = dr["Material"].ToString();
                                //QList.Enqueue(Q);
                                // mySQL.UpdateData("production_order_material", "Status", "Reject", $"production_order_material.ID = '{dr._StrVal("ID")}' and production_order_material.Material = '{dr["Material"].ToString()}'");
                            }
                        }
                        //else if(dr["Status"].ToString() == "Wait")
                        //{
                        //    if ((TankStatus)TankList[plcSvc.Mat2Tank[dr["MaterialNo"].ToString()]].Status == TankStatus.IsReady)
                        //    {
                        //        sendDosingCmd(
                        //            dr._StrVal("ID")
                        //            , plcSvc.Mat2Tank[dr._StrVal("MaterialNo")]
                        //            , dr["Quantity"].ToString()
                        //            , dr["OrderNo"].ToString()
                        //            , dr["MaterialNo"].ToString()
                        //            );
                        //    }
                        //    else
                        //    {
                        //        //QueueModel Q = new QueueModel();
                        //        //Q.ID = dr._StrVal("ID");
                        //        //Q.tank = plcSvc.Mat2Tank[dr._StrVal("MaterialNo")];
                        //        //Q.Value = dr["Quantity"].ToString();
                        //        //Q.ProductionOrder = dr["OrderNo"].ToString();
                        //        //Q.Mat = dr["MaterialNo"].ToString();
                        //        //QList.Enqueue(Q);                               
                        //    }
                        //}

                        //if (!DtCount.Contains(dr["OrderNo"].ToString()))
                        //{
                        //    DtCount.Add(dr["OrderNo"].ToString());
                        //    //this.OrderCnt++;
                        //}

                        if (temp.GetValue(3).ToString() != "None")
                        {
                            temp.SetValue("None", 3);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (temp.GetValue(3).ToString() == "None")
                        {
                            temp.SetValue($"{ex.Message}[GetdataInfirst]", 3);
                            OnSystemMsg(new SystemMsgArg($"{ex.Message}[GetdataInfirst]", Color.Red));
                            SendError(dr["MaterialNo"].ToString(), $"{ex.Message}[GetdataInfirst]");
                        }
                        //OnSystemMsg(new SystemMsgArg(ex.ToString()));
                    }
                }
            }
            //Thread.Sleep(3000);
        }

        void MonitoringThread(string ID, int Tank, string Values, string ProductionOrder, string Mat)
        {
            bool Issuscess = false;
            int zz = 0;
            TankModelX model = new TankModelX();

            ClassMySQL mySQL = new ClassMySQL(ConStr);
            mySQL.Connect();
            while (!Issuscess && isrunning) 
            {
                if(Global.SystemSvc.TankList[Tank].Status == TankStatus.ProcessStart && zz == 0)
                {
                    zz++;
                    OnSystemMsg(new SystemMsgArg("Tank Number " + (Tank + 1).ToString() + " is dosing", Color.LimeGreen));
                }
                if (model.Current != TankList[Tank].Current || model.Status != TankList[Tank].Status)
                {
                    model.Current = TankList[Tank].Current;
                    model.Status = TankList[Tank].Status;
                    OnDatachange(new TankValueChangeArg(Tank));
                }

                if (Global.SystemSvc.TankList[Tank].Status == TankStatus.IsReady)
                {
                    InMonitorDelete.Add(Mat);
                    mySQL.UpdateData("production_order_material", "Status", "Done", $"production_order_material.OrderNo = '{ProductionOrder}' and production_order_material.MaterialNo = '{Mat}';");
                    mySQL.UpdateData("production_order_material", "LastUpdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), $"production_order_material.MaterialNo = '{Mat}' and production_order_material.OrderNo = {ProductionOrder};");
                    Issuscess = true;
                    //OnSystemMsg(new SystemMsgArg("Tank Number" + (Tank + 1).ToString() + "  " + "is Done", Color.Blue));
                    //OnDataSourceRow(new SystemTableArg(Tank, false, ProductionOrder));
                    DataTable Tc = new DataTable("C");
                    Tc = mySQL.SelectData($"Select * from production_order_material where production_order_material.OrderNo = '{ProductionOrder}' and production_order_material.Status = 'Inprocess'; ");
                    if (Tc.Rows.Count == 0)
                    {
                        //this.OrderCnt--;
                        mySQL.UpdateData("production_order", "Status", "Done", $"production_order.OrderNo = '{ProductionOrder}' and production_order.ID = {ID};");//
                        mySQL.UpdateData("production_order", "FinishTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), $"production_order.OrderNo = '{ProductionOrder}' and production_order.ID = {ID};");
                    }
                }

                if (TankList[Tank].Status == TankStatus.Complete)
                {
                    InMonitorDelete.Add(Mat);
                    mySQL.UpdateData("production_order_material", "Status","Done", $"production_order_material.OrderNo = '{ProductionOrder}' and production_order_material.MaterialNo = '{Mat}';");// sent to data base dbo.material_command
                    mySQL.UpdateData("production_order_material", "LastUpdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), $"production_order_material.MaterialNo = '{Mat}' and production_order_material.OrderNo = '{ProductionOrder}';");
                    Issuscess = true;
                    OnSystemMsg(new SystemMsgArg("Tank Number" + (Tank + 1).ToString() + "  " + "is Done", Color.Blue));
                    OnDataSourceRow(new SystemTableArg(Tank, false, ProductionOrder));
                    DataTable Ctb = new DataTable("A");
                    Ctb = mySQL.SelectData($"Select * from production_order_material where production_order_material.Status = 'Inprocess' and production_order_material.OrderNo = '{ProductionOrder}';");

                    if (Ctb.Rows.Count == 0 )
                    {
                        //this.OrderCnt--;
                        mySQL.UpdateData("production_order", "Status","Done",$"production_order.OrderNo = '{ProductionOrder}' and production_order.ID = {ID};");//
                        mySQL.UpdateData("production_order", "FinishTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), $"production_order.OrderNo = '{ProductionOrder}' and production_order.ID = {ID};");
                        Issuscess = true;
                    }

                    Thread.Sleep(8 * 1000);
                    OnDatachange(new TankValueChangeArg(Tank));
                }

                if (Global.SystemSvc.TankList[Tank].Status == TankStatus.Cancel)
                {
                    InMonitorDelete.Add(Mat);
                    mySQL.UpdateData("production_order_material", "Status", "Cancel", $"production_order_material.OrderNo = '{ProductionOrder}' and production_order_material.MaterialNo = '{Mat}';");// sent to data base dbo.material_command
                    mySQL.UpdateData("production_order_material", "LastUpdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), $"production_order_material.MaterialNo = '{Mat}' and production_order_material.OrderNo = '{ProductionOrder}';");
                    Issuscess = true;
                    OnSystemMsg(new SystemMsgArg("Tank Number" + (Tank + 1).ToString() + "  " + " is Cancel", Color.Red));
                    OnDataSourceRow(new SystemTableArg(Tank, false, ProductionOrder));
                    DataTable Tb = new DataTable("B");
                    Tb = mySQL.SelectData($"Select * from production_order_material where production_order_material.OrderNo = '{ProductionOrder}' and production_order_material.Status = 'Inprocess'; ");

                    if (Tb.Rows.Count == 0)
                    {
                        //this.OrderCnt--;
                        mySQL.UpdateData("production_order", "Status", "Cancel", $"production_order.OrderNo = '{ProductionOrder}' and production_order.ID = {ID};");//
                        mySQL.UpdateData("production_order", "FinishTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), $"production_order.OrderNo = '{ProductionOrder}' and production_order.ID = {ID};");

                        DataTable TX = new DataTable("B");
                        TX = mySQL.SelectData($"Select * from production_order_material where production_order_material.OrderNo = '{ProductionOrder}' and production_order_material.Status = 'Done'; ");
                        if(TX.Rows.Count > 0)
                        {
                            mySQL.UpdateData("production_order", "Status", "Done", $"production_order.OrderNo = '{ProductionOrder}' and production_order.ID = {ID};");//
                            mySQL.UpdateData("production_order", "FinishTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), $"production_order.OrderNo = '{ProductionOrder}' and production_order.ID = {ID};");

                        }
                    }

                    Thread.Sleep(8 * 1000);
                    OnDatachange(new TankValueChangeArg(Tank));
                }

                Thread.Sleep(1000);
            }
            //if (!isrunning && !InMonitorDelete.Contains(Mat)) InMonitorDelete.Add(Mat);
            //OnSystemMsg(new SystemMsgArg("Tank Number" + (Tank + 1).ToString() + "  " + "is Done", Color.Blue));
            mySQL.DisConnect();
            InProcessMaterial.Remove(Mat);
        }

        void CkeckInprocess(int OrderNo, string Mat)
        {
            bool ii = true;
            ClassMySQL mySQL = new ClassMySQL(ConStr);
            mySQL.Connect();
            DataTable Data = new DataTable();

            while (ii)
            {
                Data = mySQL.SelectData($"Select * from production_order_material where Status = 'Inprocess' or Status = 'Wait' and production_order_material.OrderNo = '{Mat}' ;");
                if (Data.Rows.Count == 0) ii = false;
                Application.DoEvents();
                Thread.Sleep(1000);
            }
            mySQL.UpdateData("production_order", "Status", "Done", $"production_order.OrderNo = {OrderNo};");//
            mySQL.UpdateData("production_order", "FinishTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), $"production_order.OrderNo = {OrderNo};");
            mySQL.DisConnect();
        }

        void CheckDataProcess()
        {
            InProcessMaterial.Clear();
            for (int i=1; i<=20; i++)
            {
                try
                {
                    if ((TankStatus)TankList[i - 1].Status != TankStatus.IsReady)
                    {
                        InProcessMaterial.Add(plcSvc.PlcTagsRead["MatTag#" + i].ItemValue.ToString().Replace("\0", string.Empty));
                    }
                    else
                    {
                        try
                        {
                            InProcessMaterial.Remove(plcSvc.PlcTagsRead["MatTag#" + i].ItemValue.ToString().Replace("\0", string.Empty));
                        }
                        catch
                        {

                        }
                    }
                }
                catch
                {

                }
            }
        }

        public static bool AreTablesTheSame(DataTable tbl1, DataTable tbl2)
        {
            if (tbl1.Rows.Count != tbl2.Rows.Count || tbl1.Columns.Count != tbl2.Columns.Count)
                return false;


            for (int i = 0; i < tbl1.Rows.Count; i++)
            {
                for (int c = 0; c < tbl1.Columns.Count; c++)
                {
                    if (!Equals(tbl1.Rows[i][c], tbl2.Rows[i][c]))
                        return false;
                }
            }
            return true;
        }

        void sendDosingCmd(string ID, int tank, string Value, string ProductionOrder, string Mat)
        {
            bool IsSendCom = true;
            int kk = tank + 1;
            ClassMySQL mySQL = new ClassMySQL(ConStr);
            mySQL.Connect();
            int TimeReCommand = 0;

            if (!InProcessMaterial.Contains(Mat))
            {
                //plcSvc.Write(plcSvc.PlcTagsWrite["CMD_Tank#0"].ItemName, new Tag(plcSvc.PlcTagsWrite["CMD_Tank#0"].ItemName,"A30", "String"));
                //Thread.Sleep(500);
                //plcSvc.Write(plcSvc.PlcTagsWrite["CMD_Start#0"].ItemName, new Tag(plcSvc.PlcTagsWrite["CMD_Start#0"].ItemName, 0,"Bool"));
                //Thread.Sleep(500);
                plcSvc.Write(plcSvc.PlcTagsWrite["CMD_Tank#0"].ItemName, new Tag(plcSvc.PlcTagsWrite["CMD_Tank#0"].ItemName, kk, "Int"));
                Thread.Sleep(500);
                plcSvc.Write(plcSvc.PlcTagsWrite["CMD_Target#0"].ItemName, new Tag(plcSvc.PlcTagsWrite["CMD_Target#0"].ItemName, Convert.ToDouble(Value), "Float"));
                Thread.Sleep(500);
                plcSvc.Write(plcSvc.PlcTagsWrite["CMD_Start#0"].ItemName, new Tag(plcSvc.PlcTagsWrite["CMD_Start#0"].ItemName, 1, "Bool"));
                Thread.Sleep(500);

                while (IsSendCom && TimeReCommand < 15)
                {
                    IsSendCom = ((TankStatus)TankList[tank].Status != TankStatus.IsReady) ? false : true;
                    TimeReCommand++;
                    Thread.Sleep(1000);
                }

                plcSvc.Write(plcSvc.PlcTagsWrite["CMD_Start#0"].ItemName, new Tag(plcSvc.PlcTagsWrite["CMD_Start#0"].ItemName, 0, "Bool"));

                if (TimeReCommand < 15)
                {
                    SourceTable.Rows.Add(new object[] {
                            ProductionOrder,
                            Mat,
                            Value.ToString(),
                            " "
                    });

                    mySQL.UpdateData("production_order_material", "Status", "Inprocess", $"production_order_material.OrderNo = '{ProductionOrder}' and production_order_material.MaterialNo = '{Mat}';");
                    mySQL.UpdateData("production_order", "Status", "Inprocess", $"production_order.OrderNo = '{ProductionOrder}' and production_order.ID = {ID};");
                    mySQL.UpdateData("production_order", "ProcessStart", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), $"production_order.ID = {ID} and production_order.OrderNo = '{ProductionOrder}';");
                    InProcessMaterial.Add(Mat);
                    OnSystemMsg(new SystemMsgArg("Tank Number " + (tank + 1).ToString() + " is send command", Color.LimeGreen));
                    new Thread(() => MonitoringThread(ID, tank, Value, ProductionOrder, Mat)).Start();
                    OnDataSourceRow(new SystemTableArg(tank,true, ProductionOrder));
                }
                else
                {
                    //this.OrderCnt--;
                    mySQL.UpdateData("production_order_material", "Status", "reject", $"production_order_material.OrderNo = '{ProductionOrder}' and production_order_material.MaterialNo = '{Mat}';");
                    mySQL.UpdateData("production_order", "Status", "reject", $"production_order.OrderNo = '{ProductionOrder}' and production_order.ID = {ID};");
                    IsCancel.Add(ID);
                }
                mySQL.DisConnect();

                #region UpdateToDatagridView
                UpdateToDatagridView();
                #endregion

                //MonitoringThread(ID, tank, ProductionOrder, Mat);
            }
            Thread.Sleep(1000);
        }

        private void UpdateToDatagridView()
        {
            if (InMonitorDelete.Count != 0)
            {
                for (int i = 0; i < InMonitorDelete.Count; i++)
                {
                    try
                    {
                        DataRow row = SourceTable.Select($"MaterialNo = '{InMonitorDelete[i]}'").FirstOrDefault();
                        try
                        {
                            SourceTable.Rows.Remove(row);
                            InMonitorDelete.RemoveAt(i);
                        }
                        catch { }
                        if (temp.GetValue(4).ToString() != "None")
                        {
                            temp.SetValue("None", 4);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (temp.GetValue(4).ToString() == "None")
                        {
                            temp.SetValue($"Error:{ex.Message}[UpdateToDatagridView]", 4);
                            OnSystemMsg(new SystemMsgArg($"Error:{ex.Message}[UpdateToDatagridView]", Color.Red));
                            SendError("System.UpdateToDatagridView", $"Error:{ex.Message}[UpdateToDatagridView]");
                        }
                    }
                }
            }

            #region Fortest
            //if (!AreTablesTheSame(SourceTable, TableA))
            //{
            //    TableA = SourceTable.Copy();
            //    TableB = SourceTable.Copy();
            //    for (int i = 0; i < TableB.Rows.Count; i++)
            //    {
            //        TableB.Rows[i]["Current"] = Global.SystemSvc.TankList[Convert.ToInt32(plcSvc.Mat2Tank[TableB.Rows[i]["MaterialNo"].ToString()])].Current;
            //    }
            //    OnDataSourceChange(new DataSourceChangeArg(TableB));
            //    //OnMaterialProcessingDataChange(new MaterialProcessingDataChangeArg(TableB.Rows[i]["MaterialNo"].ToString()));
            //}
            #endregion
        }


        //void DataTableVaridate()
        //{
        //    for (int i = 1; i <= 20; i++)
        //    {
        //        if ((TankStatus)TankList[i - 1].Status != TankStatus.Complete)
        //        {
        //            DataRow[] rows = SourceTable.Select($"Material = '{plcSvc.PlcTagsRead["MatTag#" + i].ToString()}'");
        //            for (int j = 0; j < rows.Length; j++)
        //            {
        //                SourceTable.Rows.RemoveAt(SourceTable.Rows.IndexOf(rows[0]));
        //            }
        //        }
        //    }
        //}

        //int getIndexOfKey(Dictionary<string, TankModelTAG> tempDict, String key)
        //{
        //    int index = -1;
        //    foreach (String value in tempDict.Keys)
        //    {
        //        index++;
        //        if (key == value)
        //            return index;
        //    }
        //    return -1;
        //}

        //public void QueueDone()
        //{
        //    while(Queue.Count>0)
        //    {
        //        int Index = Queue.Dequeue();
        //        SourceTable.Rows[Index]["Current"] = "Done";
        //    }
        //}

        void systemmessage(string msg, Color c) {
            //if (this.InvokeRequired) {
            //    this.Invoke(new Action<string, Color>(this.systemmessage), new object[] { msg, c });
            //    return;
            //}

            //if (rtxt_msg.Text.Length > 2000) rtxt_msg.Text = "";
            //rtxt_msg.Text += $"{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} {msg}\r\n";
        }

        //string ck = "";
        //static int kk = 0;
        public void readvals() {

            var tags = plcSvc.ReadParameters();
            //if (tags == null)
            //{
            //    if(ck != "PLC Is not ready" && kk==1)
            //    {
            //        ck = "PLC Is not ready";
            //    }
            //    if (kk == 0) kk = 1;
            //    return;
            //}
            //else
            //{
            //    if (ck != "-")
            //    {
            //        ck = "-";
            //    }
            //}

            try
            {
                foreach (var t in tags)
                {

                    var par = t.Key.Split('#');
                    var tNum = par[1]._2int() - 1;
                    switch (par[0])
                    {
                        case "Target":
                            TankList[tNum].Target = t.Value._2db();
                            break;
                        case "CurrentWeight":
                            TankList[tNum].Current = t.Value._2db();
                            break;
                        case "RemainMaterial":
                            TankList[tNum].Remain = t.Value._2db();
                            break;
                        case "Status":
                            TankList[tNum].Status = (TankStatus)t.Value._2int();
                            break;
                        case "MatTag":
                            TankList[tNum].MatTag = t.Value.ToString().Replace("\0", string.Empty);
                            break;
                            #region note
                            //case "Start":
                            //    TankList[tNum].Start = t.Value.ToString();
                            //    break;
                            //case "Done":
                            //    TankList[tNum].Done = t.Value.ToString();
                            //    break;
                            //case "Ready":
                            //    TankList[tNum].Ready = t.Value.ToString();
                            //    break;
                            //case "PV_Mat":
                            //    TankList[tNum].PV = t.Value.ToString();
                            //    break;
                            //case "SV_Mat":
                            //    TankList[tNum].SV = t.Value.ToString();
                            //    break;
                            //case "MatID":
                            //    TankList[tNum].MatID = t.Value.ToString().Replace("\0", string.Empty);
                            //    break;
                            #endregion
                    }
                }

                if (temp.GetValue(5).ToString() != "None")
                {
                    temp.SetValue("None", 5);
                }
            }
            catch (Exception ex)
            {
                if (temp.GetValue(5).ToString() == "None")
                {
                    temp.SetValue($"{ex.Message}[readvals]", 5);
                    OnSystemMsg(new SystemMsgArg($"{ex.Message}[readvals]", Color.Red));
                    SendError("System.readvals", $"{ex.Message}[readvals]");
                }
                try
                {
                    plcSvc.Connect();
                }
                catch
                {

                }
                //OnSystemMsg(new SystemMsgArg(ex.ToString()));
            }
            
            OnReadComplete(null);
        }

        private void SendError(string Source, string Details)
        {
            ClassMySQL mySQL = new ClassMySQL(ConStr);
            mySQL.Connect();
            mySQL.InsertData("error_log", Source, Details);
            mySQL.DisConnect();
        }

        #endregion

        #region Testing
        public void InitmockPLC() {

            var rd = new Random();
            for (var t = 0; t < 20; t++) {
                try {
                    //TankList[t].PV = rd.Next(0, 100).ToString();
                    //TankList[t].SV = rd.Next(100, 200);
                    //TankList[t]. = TankList[t].Target - TankList[t].Current;
                    //TankList[t].Status = (TankStatus)(rd.Next(0, 9));
                }
                catch (Exception)
                {

                }
            }
        }

        public void mockPLCForce(int TankNuber, double Target, double Current, TankStatus Status)
        {
            //TankList[TankNuber-1].Current = Current;
            //TankList[TankNuber-1].Target = Target;
            //TankList[TankNuber-1].Remain = Target - Current;
            //TankList[TankNuber-1].Status = Status;
            //OnDatachange(new TankValueChangeArg(TankNuber-1));
        }

        #endregion
    }

    #region EventModel
    public class TankValueChangeArg:EventArgs
    {
        public int TankNumber { get; set; }
        public TankValueChangeArg (int t)
        {
            TankNumber = t;
        }
    }

    public class DataSourceChangeArg : EventArgs
    {
        public DataTable DataTable { get; set; }
        public DataSourceChangeArg(DataTable Data)
        {
            DataTable = Data;
        }
    }
    public class MaterialProcessingDataChangeArg : EventArgs
    {
        public string Material { get; set; }
        public MaterialProcessingDataChangeArg(string mat )
        {
            Material = mat;
        }
    }

    public class SystemMsgArg : EventArgs
    {
        public string Msg { get; set; }
        public Color Color { get; set; }
        public SystemMsgArg(string DMsg, Color color )
        {
            Msg = DMsg;
            Color = color;
        }
    }

    public class SystemTableArg : EventArgs
    {
        public string OrderNo { get; set; }
        public int Tank { get; set; }
        public bool AddRow { get; set; }
        public SystemTableArg(int dTank, bool dAddRow, string dOrderNo)
        {
            Tank = dTank;
            AddRow = dAddRow;
            OrderNo = dOrderNo;
        }
    }

    public class SystemLostArg : EventArgs
    {
        public SystemLostArg()
        {

        }
    }
    #endregion
}
