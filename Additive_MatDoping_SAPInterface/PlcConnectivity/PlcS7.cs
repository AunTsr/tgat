﻿using S7.Net;
using S7NetWrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Additive_MatDosing_SAPInterface.PlcConnectivity
{
    class PlcS7 {
        #region Singleton

        // For implementation refer to: http://geekswithblogs.net/BlackRabbitCoder/archive/2010/05/19/c-system.lazylttgt-and-the-singleton-design-pattern.aspx        
        private static readonly Lazy<PlcS7> _instance = new Lazy<PlcS7>(() => new PlcS7());

        public static PlcS7 Instance {
            get {
                return _instance.Value;
            }
        }

        #endregion

        #region Public properties

        public ConnectionStates ConnectionState { get { return plcDriver != null ? plcDriver.ConnectionState : ConnectionStates.Offline; } }

        public DB1 Db1 { get; set; }

        public TimeSpan CycleReadTime { get; private set; }

        #endregion

        #region Private fields

        IPlcSyncDriver plcDriver;

        System.Timers.Timer timer = new System.Timers.Timer();

        public DateTime lastReadTime;

        #endregion

        #region Constructor

        private PlcS7() {
            Db1 = new DB1();
            //timer.Interval = 100; // ms
            //timer.Elapsed += timer_Elapsed;
            //timer.Enabled = true;
            lastReadTime = DateTime.Now;
        }

        #endregion

        #region Event handlers

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {
            if (plcDriver == null || plcDriver.ConnectionState != ConnectionStates.Online) {
                return;
            }

            timer.Enabled = false;
            CycleReadTime = DateTime.Now - lastReadTime;
            try {
                RefreshTags();
            }
            finally {
                timer.Enabled = true;
                lastReadTime = DateTime.Now;
            }
        }

        #endregion

        #region Public methods

        public void Connect(string ipAddress, string cputype, short rack, short slot) {
            if (!IsValidIp(ipAddress)) {
                throw new ArgumentException("Ip address is not valid");
            }
            plcDriver = new S7NetPlcDriver(ConvType(cputype), ipAddress, rack, slot);
            plcDriver.Connect();
        }

        public void Disconnect() {
            if (plcDriver == null || this.ConnectionState == ConnectionStates.Offline) {
                return;
            }
            plcDriver.Disconnect();
        }

        public void Write(string name, object value) {
            if (plcDriver == null || plcDriver.ConnectionState != ConnectionStates.Online) {
                return;
            }
            //Tag tag = new Tag(name, value);
            Tag tag = (Tag)value;
            List<Tag> tagList = new List<Tag>();
            tagList.Add(tag);
            plcDriver.WriteItems(tagList);
        }

        public void Write(List<Tag> tags) {
            if (plcDriver == null || plcDriver.ConnectionState != ConnectionStates.Online) {
                return;
            }
            plcDriver.WriteItems(tags);
        }

        public object Read(string name) {
            if (plcDriver == null || plcDriver.ConnectionState != ConnectionStates.Online) {
                return null;
            }

            Tag tag = new Tag(name);
            List<Tag> tagList = new List<Tag>();
            tagList.Add(tag);
            return plcDriver.ReadItems(tagList)[0].ItemValue;
        }

        public List<Tag> Read(List<Tag> tags) {
            if (plcDriver == null || plcDriver.ConnectionState != ConnectionStates.Online) {
                return null;
            }
            return plcDriver.ReadItems(tags);
        }

        private CpuType ConvType(string cpu) {
            switch (cpu) {
                case "S7-200":
                    return CpuType.S7200;
                case "S7-300":
                    return CpuType.S7300;
                case "S7-400":
                    return CpuType.S7400;
                case "S7-1200":
                    return CpuType.S71200;
                case "S7-1500":
                    return CpuType.S71500;
                default:
                    return CpuType.S7300;
            }
        }

        #endregion        

        #region Private methods

        private bool IsValidIp(string addr)
        {
            IPAddress ip;
            bool valid = !string.IsNullOrEmpty(addr) && IPAddress.TryParse(addr, out ip);
            return valid;
        }

        private void RefreshTags()
        {
            plcDriver.ReadClass(Db1, 1);
        }

        #endregion

        
    }
}
