﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Additive_MatDosing_SAPInterface.Model;
using System.Diagnostics;

namespace Additive_MatDosing_SAPInterface.UI {

    public partial class ucDosingMon : UserControl {

        public int TankNum { get; set; } = 0;

        TankStatus status;
        public TankStatus Status { 
            get => status;
            set {
                status = value;
                lb_status.Text = value.ToString();
            }
        }

        public ucDosingMon() {
            InitializeComponent();
            InitData();
        }

        public void UpdateValue(double target,double val,string remain, TankStatus status) {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<double, double,string, TankStatus>(this.UpdateValue), new object[] { target, val, remain , status });
                return;
            }

            label5.Text = target.ToString();
            label6.Text = val.ToString();
            label7.Text = (target - val).ToString();
            lb_status.Text = status.ToString();
            UpdateView();
        }

        void UpdateView() {
            try
            {
                chart1.Series[0].Points.Clear();
                chart1.Series[1].Points.Clear();
                var temp = (Convert.ToDouble(label6.Text) / Convert.ToDouble(label5.Text));

                //Debug.Print(temp.ToString());
                if (temp < 0.01|| temp>1) return;
                chart1.Series[0].Points.AddXY("Dosing", temp);//percent real
                chart1.Series[1].Points.AddXY("Dosing", 1 - temp);//percent []

                chart1.Series[0].Points[0].Label = temp.ToString("00.00%");//percent real
            }
            catch { }
        }

        void InitData()
        {
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();
            chart1.Series[0].Points.AddXY("Dosing", 0);//percent real
            chart1.Series[1].Points.AddXY("Dosing", 100);//percent []
            chart1.Series[0].Points[0].Label = "00.00%";//percent real
        }

        private void ucDosingMon_Load(object sender, EventArgs e) {
            lb_tank.Text = $"Tank#{TankNum}";
        }
    }
}
