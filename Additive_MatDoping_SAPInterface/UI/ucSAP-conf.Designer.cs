﻿namespace Additive_MatDosing_SAPInterface.UI {
    partial class ucSAP_conf {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dg_tags = new System.Windows.Forms.DataGridView();
            this.XmlTags = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SystemVars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txt_saploc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_xmltag = new System.Windows.Forms.TextBox();
            this.txt_systag = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.num_cyc = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.lview_ = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txt_sapbk = new System.Windows.Forms.TextBox();
            this.rd_sap = new System.Windows.Forms.RadioButton();
            this.rd_bk = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dg_tags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_cyc)).BeginInit();
            this.SuspendLayout();
            // 
            // dg_tags
            // 
            this.dg_tags.AllowUserToAddRows = false;
            this.dg_tags.AllowUserToResizeRows = false;
            this.dg_tags.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dg_tags.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_tags.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_tags.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dg_tags.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_tags.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.XmlTags,
            this.SystemVars});
            this.dg_tags.EnableHeadersVisualStyles = false;
            this.dg_tags.Location = new System.Drawing.Point(903, 3);
            this.dg_tags.Name = "dg_tags";
            this.dg_tags.RowHeadersWidth = 51;
            this.dg_tags.RowTemplate.Height = 24;
            this.dg_tags.Size = new System.Drawing.Size(436, 645);
            this.dg_tags.TabIndex = 99;
            this.dg_tags.TabStop = false;
            // 
            // XmlTags
            // 
            this.XmlTags.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.XmlTags.HeaderText = "XmlTags";
            this.XmlTags.MinimumWidth = 6;
            this.XmlTags.Name = "XmlTags";
            this.XmlTags.Width = 200;
            // 
            // SystemVars
            // 
            this.SystemVars.HeaderText = "System Vars";
            this.SystemVars.MinimumWidth = 6;
            this.SystemVars.Name = "SystemVars";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.button3.Location = new System.Drawing.Point(206, 552);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(98, 33);
            this.button3.TabIndex = 6;
            this.button3.Text = "Add";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.button2.Location = new System.Drawing.Point(1241, 654);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(98, 33);
            this.button2.TabIndex = 7;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.btSave_Click);
            // 
            // txt_saploc
            // 
            this.txt_saploc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.txt_saploc.Font = new System.Drawing.Font("Copperplate Gothic Light", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_saploc.Location = new System.Drawing.Point(4, 58);
            this.txt_saploc.Multiline = true;
            this.txt_saploc.Name = "txt_saploc";
            this.txt_saploc.Size = new System.Drawing.Size(301, 130);
            this.txt_saploc.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label2.Location = new System.Drawing.Point(0, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 18);
            this.label2.TabIndex = 10;
            this.label2.Text = "Source";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label3.Location = new System.Drawing.Point(0, 483);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 18);
            this.label3.TabIndex = 11;
            this.label3.Text = "XML Tag:";
            // 
            // txt_xmltag
            // 
            this.txt_xmltag.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.txt_xmltag.Location = new System.Drawing.Point(116, 480);
            this.txt_xmltag.Name = "txt_xmltag";
            this.txt_xmltag.Size = new System.Drawing.Size(188, 25);
            this.txt_xmltag.TabIndex = 4;
            // 
            // txt_systag
            // 
            this.txt_systag.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.txt_systag.Location = new System.Drawing.Point(160, 516);
            this.txt_systag.Name = "txt_systag";
            this.txt_systag.Size = new System.Drawing.Size(144, 25);
            this.txt_systag.TabIndex = 5;
            this.txt_systag.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_systag_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label4.Location = new System.Drawing.Point(0, 519);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 18);
            this.label4.TabIndex = 13;
            this.label4.Text = "System Vars:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label5.Location = new System.Drawing.Point(0, 355);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 18);
            this.label5.TabIndex = 15;
            this.label5.Text = "Process:";
            // 
            // num_cyc
            // 
            this.num_cyc.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.num_cyc.Location = new System.Drawing.Point(116, 353);
            this.num_cyc.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.num_cyc.Name = "num_cyc";
            this.num_cyc.Size = new System.Drawing.Size(133, 25);
            this.num_cyc.TabIndex = 3;
            this.num_cyc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(94, 441);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 18);
            this.label6.TabIndex = 17;
            this.label6.Text = "Mapping";
            // 
            // lview_
            // 
            this.lview_.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lview_.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.lview_.Font = new System.Drawing.Font("Copperplate Gothic Bold", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lview_.HideSelection = false;
            this.lview_.LargeImageList = this.imageList1;
            this.lview_.Location = new System.Drawing.Point(311, 32);
            this.lview_.Name = "lview_";
            this.lview_.Size = new System.Drawing.Size(586, 616);
            this.lview_.SmallImageList = this.imageList1;
            this.lview_.TabIndex = 18;
            this.lview_.TabStop = false;
            this.lview_.UseCompatibleStateImageBehavior = false;
            this.lview_.View = System.Windows.Forms.View.List;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label7.Location = new System.Drawing.Point(3, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 18);
            this.label7.TabIndex = 20;
            this.label7.Text = "Backup";
            // 
            // txt_sapbk
            // 
            this.txt_sapbk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.txt_sapbk.Font = new System.Drawing.Font("Copperplate Gothic Light", 10.2F, System.Drawing.FontStyle.Italic);
            this.txt_sapbk.Location = new System.Drawing.Point(4, 217);
            this.txt_sapbk.Multiline = true;
            this.txt_sapbk.Name = "txt_sapbk";
            this.txt_sapbk.Size = new System.Drawing.Size(301, 130);
            this.txt_sapbk.TabIndex = 2;
            // 
            // rd_sap
            // 
            this.rd_sap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rd_sap.AutoSize = true;
            this.rd_sap.BackColor = System.Drawing.Color.White;
            this.rd_sap.Checked = true;
            this.rd_sap.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.rd_sap.Location = new System.Drawing.Point(700, 662);
            this.rd_sap.Name = "rd_sap";
            this.rd_sap.Size = new System.Drawing.Size(63, 22);
            this.rd_sap.TabIndex = 21;
            this.rd_sap.TabStop = true;
            this.rd_sap.Text = "SAP";
            this.rd_sap.UseVisualStyleBackColor = false;
            this.rd_sap.CheckedChanged += new System.EventHandler(this.rd_CheckedChanged);
            // 
            // rd_bk
            // 
            this.rd_bk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rd_bk.AutoSize = true;
            this.rd_bk.BackColor = System.Drawing.Color.White;
            this.rd_bk.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.rd_bk.Location = new System.Drawing.Point(802, 662);
            this.rd_bk.Name = "rd_bk";
            this.rd_bk.Size = new System.Drawing.Size(95, 22);
            this.rd_bk.TabIndex = 22;
            this.rd_bk.Text = "BackUp";
            this.rd_bk.UseVisualStyleBackColor = false;
            this.rd_bk.CheckedChanged += new System.EventHandler(this.rd_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(86, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 18);
            this.label9.TabIndex = 24;
            this.label9.Text = "SAP Data";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(307, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 18);
            this.label8.TabIndex = 25;
            this.label8.Text = "View Files";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label10.Location = new System.Drawing.Point(255, 355);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 18);
            this.label10.TabIndex = 26;
            this.label10.Text = "sec";
            // 
            // ucSAP_conf
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt_saploc);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.rd_bk);
            this.Controls.Add(this.rd_sap);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txt_sapbk);
            this.Controls.Add(this.lview_);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.num_cyc);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt_systag);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_xmltag);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dg_tags);
            this.Name = "ucSAP_conf";
            ((System.ComponentModel.ISupportInitialize)(this.dg_tags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_cyc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dg_tags;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txt_saploc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_xmltag;
        private System.Windows.Forms.TextBox txt_systag;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown num_cyc;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListView lview_;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_sapbk;
        private System.Windows.Forms.RadioButton rd_sap;
        private System.Windows.Forms.RadioButton rd_bk;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn XmlTags;
        private System.Windows.Forms.DataGridViewTextBoxColumn SystemVars;
        private System.Windows.Forms.Label label10;
    }
}
