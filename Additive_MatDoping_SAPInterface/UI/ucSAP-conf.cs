﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Additive_MatDosing_SAPInterface.Model;
using System.IO;

namespace Additive_MatDosing_SAPInterface.UI {
    public partial class ucSAP_conf : ucbaseControl {

        public ucSAP_conf() {
            InitializeComponent();
            loadConfig();
            ShowDirectoriesInListView();
        }

        private void btSave_Click(object sender, EventArgs e) {
            saveConfig();
        }
        private void btAdd_Click(object sender, EventArgs e) {
            Addtag();
        }
        private void txt_systag_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) Addtag();
        }

        void loadConfig() {
            var conf = Helper.JsontoObj<SAPConfigModel>(
                            Helper.ReadTxtfile(Helper.SAPConfig_)
                       );

            if (conf == null) return;
            txt_saploc.Text = conf.Location;
            txt_sapbk.Text = conf.Backup;
            num_cyc.Value = conf.Cycle;

            foreach (var dic in conf.TagsMapping) {
                dg_tags.Rows.Add(
                    dic.Key,
                    dic.Value
                );
            }

//=================================================================================================================================================================================
            #region load params to used class resd XMLManage.cs
            Appendix.Plus(conf.TagsMapping);
            XmlManage.PathFile = rd_sap.Checked ? txt_saploc.Text : txt_sapbk.Text;
            XmlManage.PathFileBK = rd_sap.Checked ? txt_sapbk.Text : txt_saploc.Text;
            XmlManage.range = dg_tags.Rows.Count;
            #endregion
//=================================================================================================================================================================================

        }

        void saveConfig() {
            var conf = new SAPConfigModel();
            conf.Location = txt_saploc.Text;
            conf.Backup = txt_sapbk.Text;
            conf.Cycle = num_cyc.Value;

            foreach (DataGridViewRow dr in dg_tags.Rows) {
                conf.TagsMapping.Add(dr._StrVal(0), dr._StrVal(1));
            }

            Helper.SaveTxt2file(
                Helper.SAPConfig_,
                Helper.ObjToJson(conf)
            );

            MessageBox.Show("Save success!");
        }

        void Addtag() {
            dg_tags.Rows.Add( 
                txt_xmltag.Text,
                txt_systag.Text
            );

            txt_xmltag.Text = txt_systag.Text= "";
            txt_xmltag.Focus();
        }

        private void rd_CheckedChanged(object sender, EventArgs e) {
            lview_.Clear();
            if (((RadioButton)sender).Checked)ShowDirectoriesInListView();
        }

        private void ShowDirectoriesInListView() {
            try
            {
                string path = rd_sap.Checked ? txt_saploc.Text : txt_sapbk.Text;
                //XmlManage xmlData = new XmlManage();
                //xmlData.PathFile = path;
                //xmlData.range = dg_tags.Rows.Count;
                DirectoryInfo info1 = new DirectoryInfo(path);
                FileInfo[] files = info1.GetFiles("*.XML", SearchOption.TopDirectoryOnly);
                foreach (FileInfo fi in info1.GetFiles("*.XML", SearchOption.TopDirectoryOnly))
                {
                    imageList1.Images.Add(Icon.ExtractAssociatedIcon(fi.FullName));
                    lview_.Items.Add(fi.Name, imageList1.Images.Count - 1);
                }
                #region note
                //string path = rd_sap.Checked ? txt_saploc.Text : txt_sapbk.Text;
                //DirectoryInfo info = new DirectoryInfo(path);
                //DirectoryInfo parent = info.Parent;
                //lview_.Items.Clear();
                //imageList1.Images.Clear();
                //if (parent != null)
                //{
                //    lview_.Items.Add(new ListViewItem("...", 0));
                //}

                //imageList1.Images.Add(Icon.ExtractAssociatedIcon(path));
                //foreach (DirectoryInfo dInfo in info.GetDirectories())
                //{
                //    lview_.Items.Add(new ListViewItem(dInfo.Name, 0));
                //}


                //foreach (FileInfo fi in info.GetFiles().Where(f => f.Extension == ".xml"))
                //{
                //    imageList1.Images.Add(Icon.ExtractAssociatedIcon(fi.FullName));
                //    lview_.Items.Add(fi.Name, imageList1.Images.Count - 1);
                //}


                //foreach (FileInfo fi in info.GetFiles("*.XML", SearchOption.AllDirectories))
                //{
                //    imageList1.Images.Add(Icon.ExtractAssociatedIcon(fi.FullName));
                //    lview_.Items.Add(fi.Name, imageList1.Images.Count - 1);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void test()
        {
            string path = rd_sap.Checked ? txt_saploc.Text : txt_sapbk.Text;
            //XmlManage xmlData = new XmlManage();
            //xmlData.PathFile = path;
            //xmlData.range = dg_tags.Rows.Count;
            DirectoryInfo info1 = new DirectoryInfo(path);
            FileInfo[] files = info1.GetFiles("*.XML", SearchOption.TopDirectoryOnly);
            foreach (FileInfo fi in info1.GetFiles("*.XML", SearchOption.TopDirectoryOnly))
            {
                imageList1.Images.Add(Icon.ExtractAssociatedIcon(fi.FullName));
                lview_.Items.Add(fi.Name, imageList1.Images.Count - 1);
            }
        }
    }
}
//XmlManage xmlData = new XmlManage();
//xmlData.PathFile = txt_saploc.Text;
//xmlData.fileName = fi.ToString();
//xmlData.range = dg_tags.Rows.Count;
//List<Dictionary<string, string>> testData = new List<Dictionary<string, string>>();
//testData = xmlData.GetData();