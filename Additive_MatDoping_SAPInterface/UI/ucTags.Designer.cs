﻿namespace Additive_MatDosing_SAPInterface.UI {
    partial class ucTags {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bt_save = new System.Windows.Forms.Button();
            this.dg_tags = new System.Windows.Forms.DataGridView();
            this.SystemPars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlcTags = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ReadWrite = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bt_test = new System.Windows.Forms.Button();
            this.bt_mon = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.num_cyc = new System.Windows.Forms.NumericUpDown();
            this.txt_ip = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bt_read = new System.Windows.Forms.Button();
            this.bt_write = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbx_type = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.num_rack = new System.Windows.Forms.NumericUpDown();
            this.num_slot = new System.Windows.Forms.NumericUpDown();
            this.lb_test = new System.Windows.Forms.Label();
            this.bt_reload = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dg_tags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_cyc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_rack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_slot)).BeginInit();
            this.SuspendLayout();
            // 
            // bt_save
            // 
            this.bt_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bt_save.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.bt_save.Location = new System.Drawing.Point(1241, 654);
            this.bt_save.Name = "bt_save";
            this.bt_save.Size = new System.Drawing.Size(98, 33);
            this.bt_save.TabIndex = 4;
            this.bt_save.Text = "Save";
            this.bt_save.UseVisualStyleBackColor = false;
            this.bt_save.Click += new System.EventHandler(this.Btsave_Click);
            // 
            // dg_tags
            // 
            this.dg_tags.AllowUserToAddRows = false;
            this.dg_tags.AllowUserToDeleteRows = false;
            this.dg_tags.AllowUserToResizeRows = false;
            this.dg_tags.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dg_tags.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_tags.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_tags.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_tags.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_tags.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SystemPars,
            this.PlcTags,
            this.DataType,
            this.ReadWrite,
            this.Status,
            this.Value});
            this.dg_tags.EnableHeadersVisualStyles = false;
            this.dg_tags.Location = new System.Drawing.Point(270, 3);
            this.dg_tags.Name = "dg_tags";
            this.dg_tags.RowHeadersWidth = 51;
            this.dg_tags.RowTemplate.Height = 24;
            this.dg_tags.Size = new System.Drawing.Size(1069, 645);
            this.dg_tags.TabIndex = 6;
            this.dg_tags.TabStop = false;
            // 
            // SystemPars
            // 
            this.SystemPars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SystemPars.HeaderText = "System Params";
            this.SystemPars.MinimumWidth = 6;
            this.SystemPars.Name = "SystemPars";
            this.SystemPars.ReadOnly = true;
            this.SystemPars.Width = 200;
            // 
            // PlcTags
            // 
            this.PlcTags.HeaderText = "PLC Tags";
            this.PlcTags.MinimumWidth = 6;
            this.PlcTags.Name = "PlcTags";
            // 
            // DataType
            // 
            this.DataType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.DataType.HeaderText = "DataType";
            this.DataType.Items.AddRange(new object[] {
            "Bool",
            "Int",
            "String",
            "Float"});
            this.DataType.Name = "DataType";
            // 
            // ReadWrite
            // 
            this.ReadWrite.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ReadWrite.DefaultCellStyle = dataGridViewCellStyle2;
            this.ReadWrite.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.ReadWrite.DisplayStyleForCurrentCellOnly = true;
            this.ReadWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReadWrite.HeaderText = "R/W";
            this.ReadWrite.Items.AddRange(new object[] {
            "Read",
            "Write"});
            this.ReadWrite.MinimumWidth = 6;
            this.ReadWrite.Name = "ReadWrite";
            this.ReadWrite.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ReadWrite.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ReadWrite.Width = 125;
            // 
            // Status
            // 
            this.Status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Status.DefaultCellStyle = dataGridViewCellStyle3;
            this.Status.HeaderText = "Status";
            this.Status.MinimumWidth = 6;
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 125;
            // 
            // Value
            // 
            this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Value.DefaultCellStyle = dataGridViewCellStyle4;
            this.Value.HeaderText = "Value";
            this.Value.MinimumWidth = 6;
            this.Value.Name = "Value";
            this.Value.Width = 150;
            // 
            // bt_test
            // 
            this.bt_test.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.bt_test.Location = new System.Drawing.Point(166, 211);
            this.bt_test.Name = "bt_test";
            this.bt_test.Size = new System.Drawing.Size(98, 33);
            this.bt_test.TabIndex = 8;
            this.bt_test.Text = "Test";
            this.bt_test.UseVisualStyleBackColor = false;
            this.bt_test.Click += new System.EventHandler(this.bt_test_Click);
            // 
            // bt_mon
            // 
            this.bt_mon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bt_mon.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.bt_mon.Location = new System.Drawing.Point(538, 654);
            this.bt_mon.Name = "bt_mon";
            this.bt_mon.Size = new System.Drawing.Size(157, 33);
            this.bt_mon.TabIndex = 9;
            this.bt_mon.Text = "Monitoring";
            this.bt_mon.UseVisualStyleBackColor = false;
            this.bt_mon.Click += new System.EventHandler(this.bt_mon_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label10.Location = new System.Drawing.Point(210, 157);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 18);
            this.label10.TabIndex = 28;
            this.label10.Text = "sec";
            // 
            // num_cyc
            // 
            this.num_cyc.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.num_cyc.Location = new System.Drawing.Point(109, 155);
            this.num_cyc.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.num_cyc.Name = "num_cyc";
            this.num_cyc.Size = new System.Drawing.Size(95, 25);
            this.num_cyc.TabIndex = 27;
            this.num_cyc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_cyc.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // txt_ip
            // 
            this.txt_ip.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.txt_ip.Location = new System.Drawing.Point(109, 9);
            this.txt_ip.Name = "txt_ip";
            this.txt_ip.Size = new System.Drawing.Size(155, 25);
            this.txt_ip.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label3.Location = new System.Drawing.Point(65, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 18);
            this.label3.TabIndex = 30;
            this.label3.Text = "IP:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label1.Location = new System.Drawing.Point(29, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 18);
            this.label1.TabIndex = 31;
            this.label1.Text = "Rack:";
            // 
            // bt_read
            // 
            this.bt_read.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bt_read.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.bt_read.Location = new System.Drawing.Point(270, 654);
            this.bt_read.Name = "bt_read";
            this.bt_read.Size = new System.Drawing.Size(128, 33);
            this.bt_read.TabIndex = 33;
            this.bt_read.Text = "Read";
            this.bt_read.UseVisualStyleBackColor = false;
            this.bt_read.Click += new System.EventHandler(this.bt_read_Click);
            // 
            // bt_write
            // 
            this.bt_write.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bt_write.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.bt_write.Location = new System.Drawing.Point(404, 654);
            this.bt_write.Name = "bt_write";
            this.bt_write.Size = new System.Drawing.Size(128, 33);
            this.bt_write.TabIndex = 34;
            this.bt_write.Text = "Write";
            this.bt_write.UseVisualStyleBackColor = false;
            this.bt_write.Click += new System.EventHandler(this.bt_write_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label2.Location = new System.Drawing.Point(37, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 18);
            this.label2.TabIndex = 35;
            this.label2.Text = "Slot:";
            // 
            // cbx_type
            // 
            this.cbx_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbx_type.FormattingEnabled = true;
            this.cbx_type.Items.AddRange(new object[] {
            "S7-200",
            "S7-300",
            "S7-400",
            "S7-1200",
            "S7-1500"});
            this.cbx_type.Location = new System.Drawing.Point(109, 45);
            this.cbx_type.Name = "cbx_type";
            this.cbx_type.Size = new System.Drawing.Size(155, 26);
            this.cbx_type.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label4.Location = new System.Drawing.Point(3, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 18);
            this.label4.TabIndex = 38;
            this.label4.Text = "S7 Type:";
            // 
            // num_rack
            // 
            this.num_rack.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.num_rack.Location = new System.Drawing.Point(109, 83);
            this.num_rack.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.num_rack.Name = "num_rack";
            this.num_rack.Size = new System.Drawing.Size(155, 25);
            this.num_rack.TabIndex = 39;
            this.num_rack.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // num_slot
            // 
            this.num_slot.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.num_slot.Location = new System.Drawing.Point(109, 119);
            this.num_slot.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.num_slot.Name = "num_slot";
            this.num_slot.Size = new System.Drawing.Size(155, 25);
            this.num_slot.TabIndex = 40;
            this.num_slot.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_slot.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lb_test
            // 
            this.lb_test.BackColor = System.Drawing.Color.White;
            this.lb_test.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.lb_test.Location = new System.Drawing.Point(7, 216);
            this.lb_test.Name = "lb_test";
            this.lb_test.Size = new System.Drawing.Size(153, 23);
            this.lb_test.TabIndex = 41;
            this.lb_test.Text = "-";
            this.lb_test.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bt_reload
            // 
            this.bt_reload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bt_reload.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.bt_reload.Location = new System.Drawing.Point(1137, 654);
            this.bt_reload.Name = "bt_reload";
            this.bt_reload.Size = new System.Drawing.Size(98, 33);
            this.bt_reload.TabIndex = 42;
            this.bt_reload.Text = "Load";
            this.bt_reload.UseVisualStyleBackColor = false;
            this.bt_reload.Click += new System.EventHandler(this.bt_reload_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            this.label5.Location = new System.Drawing.Point(20, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 18);
            this.label5.TabIndex = 43;
            this.label5.Text = "Cycle:";
            // 
            // ucTags
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bt_reload);
            this.Controls.Add(this.lb_test);
            this.Controls.Add(this.num_slot);
            this.Controls.Add(this.num_rack);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbx_type);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bt_write);
            this.Controls.Add(this.bt_read);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_ip);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.num_cyc);
            this.Controls.Add(this.bt_mon);
            this.Controls.Add(this.bt_test);
            this.Controls.Add(this.dg_tags);
            this.Controls.Add(this.bt_save);
            this.Name = "ucTags";
            ((System.ComponentModel.ISupportInitialize)(this.dg_tags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_cyc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_rack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_slot)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bt_save;
        private System.Windows.Forms.DataGridView dg_tags;
        private System.Windows.Forms.Button bt_test;
        private System.Windows.Forms.Button bt_mon;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown num_cyc;
        private System.Windows.Forms.TextBox txt_ip;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bt_read;
        private System.Windows.Forms.Button bt_write;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbx_type;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown num_rack;
        private System.Windows.Forms.NumericUpDown num_slot;
        private System.Windows.Forms.Label lb_test;
        private System.Windows.Forms.Button bt_reload;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn SystemPars;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlcTags;
        private System.Windows.Forms.DataGridViewComboBoxColumn DataType;
        private System.Windows.Forms.DataGridViewComboBoxColumn ReadWrite;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
    }
}
