﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Additive_MatDosing_SAPInterface.UI {
    public partial class ucbaseControl : UserControl {
        public ucbaseControl() {
            InitializeComponent();
        }

        public void CloseUC() {
            this.ClearService();
            this.Controls.Clear();

            this.Parent.Controls.Remove(this);
            this.Dispose();
        }

        public void ClearUC() {
            this.ClearService();
            this.Controls.Clear();
        }

        virtual public void ClearService() {

        }

        private void ucbaseControl_Load(object sender, EventArgs e) {
            //this.Dock = DockStyle.Fill;
        }
    }
}
