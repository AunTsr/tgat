﻿using System;
using System.Collections.Generic;
using System.Linq;
using Additive_MatDosing_SAPInterface.services;
using System.Threading;
using System.Data;
using Additive_MatDosing_SAPInterface.Model;
using System.Diagnostics;
using System.Drawing;

namespace Additive_MatDosing_SAPInterface.UI {
    public partial class ucHome : ucbaseControl {

        List<ucDosingMon> lstDosing = new List<ucDosingMon>();
        private DataTable Table = new DataTable();
    
        public ucHome() {
            InitializeComponent();
            initTagMoldel();
            initTank();
            UpDateValue();

            Global.SystemSvc.Logging = chk_logging.Checked;
            Global.SystemSvc.ServiceStatusChange += SystemSvc_ServiceStatusChange;
            Global.SystemSvc.bgStarted += SystemSvc_bgStarted;
            Global.SystemSvc.bgStopped += SystemSvc_bgStopped;
            Global.SystemSvc.Datachange += SystemSvc_OnDatachange;
            Global.SystemSvc.DataSourceChange += SystemSvc_OnDataSourceChange;
            Global.SystemSvc.SystemMsg += SystemSvc_OnSystemMsg;
            Global.SystemSvc.TableMange += SystemSvc_OnTableMange;
            Global.SystemSvc.LostConnect += SystemSvc_OnLostConnect;

            bt_start.Enabled = true;
            bt_stop.Enabled = false;
        }
        #region event
        private void SystemSvc_bgStopped(object sender, EventArgs e)
        {
            btEnableStartStop(false);          
        }
        private void SystemSvc_OnDatachange(object sender, TankValueChangeArg e)
        {
            UpDateValue(e.TankNumber);
        }

        private void SystemSvc_bgStarted(object sender, EventArgs e)
        {
            btEnableStartStop(true);
        }

        private void SystemSvc_ServiceStatusChange(object sender, EventArgs e) {
            //throw new NotImplementedException();
        }

        private void SystemSvc_OnDataSourceChange(object sender, DataSourceChangeArg e)
        {
            UpDateDataSource(e.DataTable);
        }

        private void SystemSvc_OnSystemMsg(object sender, SystemMsgArg e)
        {
            UpDateSystemMsg(e.Msg, e.Color);
            UpDateMsgStatus(e.Msg);
        }

        private void SystemSvc_OnTableMange(object sender, SystemTableArg e)
        {
            UpdateDgView(e.Tank, e.AddRow, e.OrderNo);
        }

        private void SystemSvc_OnLostConnect(object sender, SystemLostArg e)
        {
            UpDateValue();
        }
        #endregion

        #region init event
        void initTank() {

            //ForTest
            //Global.SystemSvc.InitmockPLC();

            foreach (ucDosingMon mon in flowLayoutPanel1.Controls.OfType<ucDosingMon>()) {
                lstDosing.Add(mon);
            }
        }
        void initTagMoldel()
        {
            Global.SystemSvc.InitPLCTagmodel();
            Global.SystemSvc.readvals();
            plcSvc.SetTankMat();
        }

        void UpDateValue()
        {
            //todo read current infomation for each tank from PLC
            for (int tnk = 0; tnk < 20; tnk++)
            {
                var tmp = Global.SystemSvc.TankList[tnk];
                //lstDosing[tnk].UpdateValue(tmp.Target, tmp.Current, tmp.Remain.ToString(), tmp.Status);
                lstDosing[tnk].Status = tmp.Status;
            }
        }
        void UpDateValue(int tnk)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<int>(this.UpDateValue), new object[] { tnk });
                return;
            }

            //todo read current infomation for each tank from PLC
            var tmp = Global.SystemSvc.TankList[tnk];
            if (tmp.Status == TankStatus.IsReady)
            {
                lstDosing[tnk].UpdateValue( 0, 0 , "0", tmp.Status);
            }
            else if (tmp.Status == TankStatus.Cancel)
            {
                lstDosing[tnk].UpdateValue(0, 0, "0", tmp.Status);
            }
            else
            {
                lstDosing[tnk].UpdateValue(tmp.Target, tmp.Current, tmp.Remain.ToString(), tmp.Status);
                GridRowDataChange(Global.SystemSvc.TankList[tnk].MatTag, Global.SystemSvc.TankList[tnk].Current);
            }
        }
        #endregion

        List<string> matrow = new List<string>();
        void UpDateDataSource(DataTable dataTable)
        {
            matrow.Clear();
            while (!this.IsHandleCreated) 
            System.Threading.Thread.Sleep(100);
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<DataTable>(this.UpDateDataSource), new object[] { dataTable });
                return;
            }

            DataTable data = new DataTable();
            data = dataTable.Copy();
            //if (data.Rows.Count !=0)
            //{
            dg_tags.DataSource = null;
            dg_tags.DataSource = data;
            Table = dataTable.Copy();
                //matrow = data.AsEnumerable().Select(r => r["MaterialNo"].ToString()).ToList();
            //}

            //if (dg_tags.Rows.Count != 0)
            //{
            //    dg_tags.DataSource = null; //dg_tags.Rows.Clear();
            //    dg_tags.Refresh();
            //}
            //if (dg_tags.Columns.Count != 0)
            //{
            //    dg_tags.Columns.Clear();
            //    dg_tags.DataSource = data;
            //    matrow = data.AsEnumerable().Select(r => r["MaterialNo"].ToString()).ToList();
            //}
        }

        public void btEnableStartStop(bool isStart)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<bool>(this.btEnableStartStop), new object[] { isStart });
                return;
            }

            bt_start.Enabled = !isStart;
            bt_stop.Enabled = isStart;
        }

        public override void ClearService() {

        }

        private void bt_start_Click(object sender, EventArgs e) {
            Global.SystemSvc.Start();
        }

        private void bt_stop_Click(object sender, EventArgs e) {
            Global.SystemSvc.Stop();
        }

        private void chk_logging_CheckedChanged(object sender, EventArgs e) {
            Global.SystemSvc.Logging = chk_logging.Checked;
        }

        private void UcHome_Load(object sender, EventArgs e)
        {
            dg_tags.Columns.Clear();
            bt_start.Enabled = !Global.SystemSvc.IsRunning;
            bt_stop.Enabled = Global.SystemSvc.IsRunning;
            LoadStrindToRichText(Global.SystemSvc.lstMsg, Global.SystemSvc.lstColor);
            label2.Text = Global.SystemSvc.NowMsg;
            if (Global.SystemSvc.IsRunning)
            {
                dg_tags.DataSource = Global.SystemSvc.SourceTable;
                Table = Global.SystemSvc.SourceTable.Copy();
            }

        }

        private void GridRowDataChange(string Mat, double Current)
        {
            try
            {
                //dg_tags.Rows[matrow.IndexOf(Mat)].Cells["Current"].Value = Current;
                for (int i = 0; i < dg_tags.Rows.Count; i++)
                {
                    if (string.Equals(dg_tags.Rows[i].Cells["MaterialNo"].Value.ToString() as string, Mat))
                    {
                        dg_tags.Rows[i].Cells["Current"].Value = Current;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void UpDateSystemMsg(string Msg, Color color)
        {
            while (!this.IsHandleCreated)
                System.Threading.Thread.Sleep(100);
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<string, Color>(this.UpDateSystemMsg), new object[] { Msg, color });
                return;
            }
            string magss = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " >>> " + Msg;
            //Global.SystemSvc.ArrMsg.Add(Msg);
            Global.SystemSvc.lstMsg.Add(magss);
            Global.SystemSvc.lstColor.Add(color);
            Font font = new Font("Tahoma", 8, FontStyle.Regular);
            rtxt_msg.SelectionFont = font;
            rtxt_msg.SelectionColor = color;
            rtxt_msg.SelectedText = Environment.NewLine + magss;

            //Global.SystemSvc.SysMsg = Global.SystemSvc.SysMsg + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " >>> " + Msg +"\r\n";
            //rtxt_msg.ForeColor = color;
            //rtxt_msg.Text = Global.SystemSvc.SysMsg;
        }

        private void UpDateMsgStatus(string Msg)
        {
            while (!this.IsHandleCreated)
                System.Threading.Thread.Sleep(100);
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<string>(this.UpDateMsgStatus), new object[] { Msg });
                return;
            }

            Global.SystemSvc.NowMsg = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " >>> " + Msg;
            label2.Text = Global.SystemSvc.NowMsg;
        }

        private void LoadStrindToRichText(List<string> Msgs , List<Color> colors)
        {
            for(int i =0; i < Msgs.Count;i++)
            {
                Font font = new Font("Tahoma", 8, FontStyle.Regular);
                rtxt_msg.SelectionFont = font;
                rtxt_msg.SelectionColor = colors[i];
                rtxt_msg.SelectedText = Environment.NewLine + Msgs[i];
            }
        }

        private void UpdateDgView(int Tank, bool IsAdd, string OrderNo)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action<int, bool, string>(this.UpdateDgView), new object[] { Tank, IsAdd, OrderNo });
                return;
            }
            if (IsAdd)
            {
                //object[] newRow = { OrderNo, Global.SystemSvc.TankList[Tank].MatTag, (Global.SystemSvc.TankList[Tank].Target).ToString(), (Global.SystemSvc.TankList[Tank].Current).ToString() };
                Table.Rows.Add(new object[] { OrderNo, Global.SystemSvc.TankList[Tank].MatTag, (Global.SystemSvc.TankList[Tank].Target).ToString(), (Global.SystemSvc.TankList[Tank].Current).ToString() });
                dg_tags.DataSource = Table;
                //dg_tags.Rows.Add(OrderNo, Global.SystemSvc.TankList[Tank].MatTag, (Global.SystemSvc.TankList[Tank].Target).ToString(), (Global.SystemSvc.TankList[Tank].Current).ToString());
            }
            else
            {
                try
                {
                    DataRow row = Table.Select($"MaterialNo = '{Global.SystemSvc.TankList[Tank].MatTag}'").FirstOrDefault();
                    Table.Rows.Remove(row);
                    dg_tags.DataSource = Table;
                }
                catch(Exception ex)
                {

                }
            }
        }

        private void chk_atstart_CheckedChanged(object sender, EventArgs e)
        {
            Global.SystemSvc.IsRunning = true;
            Global.SystemSvc.bgservice();
        }
    }
}
