﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Additive_MatDosing_SAPInterface.Model;
using Additive_MatDosing_SAPInterface.services;
using S7NetWrapper;
using System.Threading;

namespace Additive_MatDosing_SAPInterface.UI {
    public partial class ucTags : ucbaseControl {
        bool Ismonitoring;
        bool ThrdStop = true;
        public ucTags() {
            InitializeComponent();
            loadConfig();
            //plcSvc.LoadConfig();
            //plcSvc.IsReady = false;
            //init();
        }

        private void Btsave_Click(object sender, EventArgs e) {
            saveConfig();
            loadConfig();
            plcSvc.LoadConfig();
        }
        private void bt_reload_Click(object sender, EventArgs e) {
            loadConfig();
            plcSvc.LoadConfig();
        }
        void loadConfig() {
            dg_tags.Rows.Clear();
            var conf = Helper.JsontoObj<TagsConfigModel>(
                            Helper.ReadTxtfile(Helper.TagsConfig_)
                       );

            if (conf == null) return;
            txt_ip.Text=conf.IpAddress;
            cbx_type.Text = conf.Cputype;
            num_rack.Value = conf.Rack;
            num_slot.Value = conf.Slot;
            num_cyc.Value=conf.Cycle;
            foreach (var tags in conf.Tags) {
                var lastindex = dg_tags.Rows.Add(
                    tags.SystemParameter,
                    tags.PLCTagsAddress.ItemName,
                    tags.PLCTagsAddress.Datatype,
                    tags.ReadWrite,
                    "NA",
                    "NA"
                );

                if(tags.ReadWrite == "Read") {
                    dg_tags.Rows[lastindex].Cells["Status"].ReadOnly = true;
                    dg_tags.Rows[lastindex].Cells["Status"].Style.BackColor = Color.Silver;
                    dg_tags.Rows[lastindex].Cells["ReadWrite"].Style.BackColor = Color.Yellow;
                }
                else {
                    dg_tags.Rows[lastindex].Cells["ReadWrite"].Style.BackColor = Color.LimeGreen;
                }
            }
        }

        void saveConfig() {
            var conf = new TagsConfigModel();

            conf.IpAddress = txt_ip.Text;
            conf.Cputype = cbx_type.Text;
            conf.Rack = (short)num_rack.Value;
            conf.Slot = (short)num_slot.Value;
            conf.Cycle = num_cyc.Value;
            foreach (DataGridViewRow dr in dg_tags.Rows) {
                conf.Tags.Add(new TagsMappingMoldel() {
                    SystemParameter = dr._StrVal(0),
                    PLCTagsAddress = new Tag(dr._StrVal(1),null, dr._StrVal(2)),
                    ReadWrite = dr._StrVal(3)
                }
                ) ; 
            }

            Helper.SaveTxt2file(
                Helper.TagsConfig_,
                Helper.ObjToJson(conf)
            );

            MessageBox.Show("Save success!");
        }

        private void bt_test_Click(object sender, EventArgs e) {
            if (plcSvc.Connect(txt_ip.Text,cbx_type.Text,(short)num_rack.Value, (short)num_slot.Value)) {
                lb_test.ForeColor = Color.LimeGreen;
                lb_test.Text = "Success";
            }
            else {
                lb_test.ForeColor = Color.Red;
                lb_test.Text = "Failed!";
            }
        }
        private void bt_read_Click(object sender, EventArgs e) {
            if(!plcSvc.IsReady) plcSvc.Connect();
            readparameters();
        }
        private void bt_write_Click(object sender, EventArgs e) {
            if (!plcSvc.IsReady) plcSvc.Connect();
            var tags = new List<Tag>();
            foreach (DataGridViewRow dr in dg_tags.Rows) {
                try
                {
                    if (dr.Cells["ReadWrite"].Value.ToString() == "Write")
                    {
                        object val;
                        switch (dr.Cells["DataType"].Value)
                        {
                            case "Float":
                                val = Convert.ToDouble(dr.Cells["Value"].Value);
                                break;
                            case "String":
                                val = dr.Cells["Value"].Value.ToString();
                                break;
                            case "Int":
                                val = Convert.ToInt32(dr.Cells["Value"].Value);
                                break;
                            case "Bool":
                                if (dr.Cells["Value"].Value.ToString() == "0") val = false;
                                if (dr.Cells["Value"].Value.ToString() == "1") val = true;
                                else val = dr.Cells["Value"].Value;
                                break;
                            default:
                                val = dr.Cells["Value"].Value;
                                break;
                                //case "Bool":
                                //    val = Convert.ToBoolean(dr.Cells["DataType"].Value.ToString());
                                //    break;
                        }
                        tags.Add(new Tag(dr.Cells["PlcTags"].Value.ToString(), val, dr.Cells["DataType"].Value.ToString()));
                    }
                }
                catch
                {
                    //Is not input format
                }
            }
            plcSvc.WriteParameters(tags);
        }
        private void bt_mon_Click(object sender, EventArgs e) {
            if (!plcSvc.IsReady) plcSvc.Connect();
            if (!Ismonitoring && ThrdStop) {
                Ismonitoring = true;
                bt_mon.Text = "Stop";
                new Task(() => bgMonitoring()).Start();
            }
            else if(!ThrdStop)
            {
                MessageBox.Show("Thread still running!, please wait...");
            }
            else {
                Ismonitoring = false;
            }
        }

        private void bgMonitoring() {
            var sleep = (int)num_cyc.Value * 1000;
            ThrdStop = false;
            while (Ismonitoring) {
                readparameters();
                Thread.Sleep(sleep);
            }
            bt_mon.Text = "Monitoring";
            ThrdStop = true;
        }
        void readparameters() {
            if (this.InvokeRequired) {
                this.Invoke(new Action(this.readparameters), null);
                return;
            }

            var readvals = plcSvc.ReadParameters();
            if (readvals == null) return;
            foreach (DataGridViewRow r in dg_tags.Rows) {
                if (r.Cells["ReadWrite"].Value.ToString() == "Read")
                    r.Cells["Value"].Value = readvals[r.Cells["SystemPars"].Value.ToString()];
            }
        }
        override public void ClearService() {
            Ismonitoring = false;
        }




        //private void Bt_conn_Click(object sender, EventArgs e)
        //{
        //    plcSvc.Connect();
        //}
        //void init() {
        //    dg_tags.Rows.Add("TankID");
        //    dg_tags.Rows.Add("DosingWeight");
        //    dg_tags.Rows.Add("CurrentWeight");
        //    dg_tags.Rows.Add("Status");
        //    dg_tags.Rows.Add("Start");
        //    dg_tags.Rows.Add("Stop");
        //    dg_tags.Rows.Add("RemainMaterial#1");
        //    dg_tags.Rows.Add("RemainMaterial#2");
        //    dg_tags.Rows.Add("RemainMaterial#3");
        //    dg_tags.Rows.Add("RemainMaterial#4");
        //    dg_tags.Rows.Add("RemainMaterial#5");
        //    dg_tags.Rows.Add("RemainMaterial#6");
        //    dg_tags.Rows.Add("RemainMaterial#7");
        //    dg_tags.Rows.Add("RemainMaterial#8");
        //    dg_tags.Rows.Add("RemainMaterial#9");
        //    dg_tags.Rows.Add("RemainMaterial#10");
        //    dg_tags.Rows.Add("RemainMaterial#11");
        //    dg_tags.Rows.Add("RemainMaterial#12");
        //    dg_tags.Rows.Add("RemainMaterial#13");
        //    dg_tags.Rows.Add("RemainMaterial#14");
        //    dg_tags.Rows.Add("RemainMaterial#15");
        //    dg_tags.Rows.Add("RemainMaterial#16");
        //    dg_tags.Rows.Add("RemainMaterial#17");
        //    dg_tags.Rows.Add("RemainMaterial#18");
        //    dg_tags.Rows.Add("RemainMaterial#19");
        //    dg_tags.Rows.Add("RemainMaterial#20");
        //}
    }
}
