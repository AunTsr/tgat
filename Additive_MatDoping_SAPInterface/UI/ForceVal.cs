﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Additive_MatDosing_SAPInterface.Model;

namespace Additive_MatDosing_SAPInterface.UI
{
    public partial class ForceVal : Form
    {
        public ForceVal()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int Tank = Convert.ToInt32(comboBox1.Text);
            double target = Convert.ToDouble(textBox2.Text);
            double current = Convert.ToDouble(textBox1.Text);
            TankStatus tankstatus = (TankStatus)comboBox2.SelectedIndex;
            Global.SystemSvc.mockPLCForce(Tank, target, current, tankstatus);
        }
    }
}
