﻿namespace Additive_MatDosing_SAPInterface.UI {
    partial class ucHome {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucHome));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rtxt_msg = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ucDosingMon1 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon2 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon3 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon4 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon5 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon6 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon7 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon8 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.ucDosingMon9 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon10 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon11 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon12 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon13 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon14 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon15 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon20 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon16 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon17 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon18 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.ucDosingMon19 = new Additive_MatDosing_SAPInterface.UI.ucDosingMon();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chk_atstart = new System.Windows.Forms.CheckBox();
            this.bt_stop = new System.Windows.Forms.Button();
            this.bt_start = new System.Windows.Forms.Button();
            this.chk_logging = new System.Windows.Forms.CheckBox();
            this.dg_tags = new System.Windows.Forms.DataGridView();
            this.orderid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.target = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.current = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_tags)).BeginInit();
            this.SuspendLayout();
            // 
            // rtxt_msg
            // 
            this.rtxt_msg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rtxt_msg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.rtxt_msg.Location = new System.Drawing.Point(3, 290);
            this.rtxt_msg.Name = "rtxt_msg";
            this.rtxt_msg.ReadOnly = true;
            this.rtxt_msg.Size = new System.Drawing.Size(445, 372);
            this.rtxt_msg.TabIndex = 0;
            this.rtxt_msg.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(-1, 264);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(152, 18);
            this.label9.TabIndex = 25;
            this.label9.Text = "System message";
            // 
            // ucDosingMon1
            // 
            this.ucDosingMon1.BackColor = System.Drawing.Color.White;
            this.ucDosingMon1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon1.Location = new System.Drawing.Point(3, 3);
            this.ucDosingMon1.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon1.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon1.Name = "ucDosingMon1";
            this.ucDosingMon1.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon1.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon1.TabIndex = 28;
            this.ucDosingMon1.TankNum = 1;
            // 
            // ucDosingMon2
            // 
            this.ucDosingMon2.BackColor = System.Drawing.Color.White;
            this.ucDosingMon2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon2.Location = new System.Drawing.Point(174, 3);
            this.ucDosingMon2.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon2.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon2.Name = "ucDosingMon2";
            this.ucDosingMon2.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon2.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon2.TabIndex = 29;
            this.ucDosingMon2.TankNum = 2;
            // 
            // ucDosingMon3
            // 
            this.ucDosingMon3.BackColor = System.Drawing.Color.White;
            this.ucDosingMon3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon3.Location = new System.Drawing.Point(345, 3);
            this.ucDosingMon3.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon3.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon3.Name = "ucDosingMon3";
            this.ucDosingMon3.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon3.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon3.TabIndex = 30;
            this.ucDosingMon3.TankNum = 3;
            // 
            // ucDosingMon4
            // 
            this.ucDosingMon4.BackColor = System.Drawing.Color.White;
            this.ucDosingMon4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon4.Location = new System.Drawing.Point(516, 3);
            this.ucDosingMon4.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon4.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon4.Name = "ucDosingMon4";
            this.ucDosingMon4.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon4.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon4.TabIndex = 31;
            this.ucDosingMon4.TankNum = 4;
            // 
            // ucDosingMon5
            // 
            this.ucDosingMon5.BackColor = System.Drawing.Color.White;
            this.ucDosingMon5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon5.Location = new System.Drawing.Point(687, 3);
            this.ucDosingMon5.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon5.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon5.Name = "ucDosingMon5";
            this.ucDosingMon5.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon5.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon5.TabIndex = 32;
            this.ucDosingMon5.TankNum = 5;
            // 
            // ucDosingMon6
            // 
            this.ucDosingMon6.BackColor = System.Drawing.Color.White;
            this.ucDosingMon6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon6.Location = new System.Drawing.Point(3, 149);
            this.ucDosingMon6.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon6.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon6.Name = "ucDosingMon6";
            this.ucDosingMon6.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon6.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon6.TabIndex = 33;
            this.ucDosingMon6.TankNum = 6;
            // 
            // ucDosingMon7
            // 
            this.ucDosingMon7.BackColor = System.Drawing.Color.White;
            this.ucDosingMon7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon7.Location = new System.Drawing.Point(174, 149);
            this.ucDosingMon7.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon7.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon7.Name = "ucDosingMon7";
            this.ucDosingMon7.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon7.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon7.TabIndex = 34;
            this.ucDosingMon7.TankNum = 7;
            // 
            // ucDosingMon8
            // 
            this.ucDosingMon8.BackColor = System.Drawing.Color.White;
            this.ucDosingMon8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon8.Location = new System.Drawing.Point(345, 149);
            this.ucDosingMon8.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon8.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon8.Name = "ucDosingMon8";
            this.ucDosingMon8.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon8.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon8.TabIndex = 35;
            this.ucDosingMon8.TankNum = 8;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon1);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon2);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon3);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon4);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon5);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon6);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon7);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon8);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon9);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon10);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon11);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon12);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon13);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon14);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon15);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon20);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon16);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon17);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon18);
            this.flowLayoutPanel1.Controls.Add(this.ucDosingMon19);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(453, 5);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(882, 593);
            this.flowLayoutPanel1.TabIndex = 36;
            // 
            // ucDosingMon9
            // 
            this.ucDosingMon9.BackColor = System.Drawing.Color.White;
            this.ucDosingMon9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon9.Location = new System.Drawing.Point(516, 149);
            this.ucDosingMon9.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon9.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon9.Name = "ucDosingMon9";
            this.ucDosingMon9.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon9.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon9.TabIndex = 36;
            this.ucDosingMon9.TankNum = 9;
            // 
            // ucDosingMon10
            // 
            this.ucDosingMon10.BackColor = System.Drawing.Color.White;
            this.ucDosingMon10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon10.Location = new System.Drawing.Point(687, 149);
            this.ucDosingMon10.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon10.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon10.Name = "ucDosingMon10";
            this.ucDosingMon10.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon10.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon10.TabIndex = 37;
            this.ucDosingMon10.TankNum = 10;
            // 
            // ucDosingMon11
            // 
            this.ucDosingMon11.BackColor = System.Drawing.Color.White;
            this.ucDosingMon11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon11.Location = new System.Drawing.Point(3, 295);
            this.ucDosingMon11.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon11.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon11.Name = "ucDosingMon11";
            this.ucDosingMon11.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon11.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon11.TabIndex = 38;
            this.ucDosingMon11.TankNum = 11;
            // 
            // ucDosingMon12
            // 
            this.ucDosingMon12.BackColor = System.Drawing.Color.White;
            this.ucDosingMon12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon12.Location = new System.Drawing.Point(174, 295);
            this.ucDosingMon12.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon12.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon12.Name = "ucDosingMon12";
            this.ucDosingMon12.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon12.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon12.TabIndex = 39;
            this.ucDosingMon12.TankNum = 12;
            // 
            // ucDosingMon13
            // 
            this.ucDosingMon13.BackColor = System.Drawing.Color.White;
            this.ucDosingMon13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon13.Location = new System.Drawing.Point(345, 295);
            this.ucDosingMon13.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon13.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon13.Name = "ucDosingMon13";
            this.ucDosingMon13.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon13.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon13.TabIndex = 40;
            this.ucDosingMon13.TankNum = 13;
            // 
            // ucDosingMon14
            // 
            this.ucDosingMon14.BackColor = System.Drawing.Color.White;
            this.ucDosingMon14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon14.Location = new System.Drawing.Point(516, 295);
            this.ucDosingMon14.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon14.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon14.Name = "ucDosingMon14";
            this.ucDosingMon14.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon14.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon14.TabIndex = 41;
            this.ucDosingMon14.TankNum = 14;
            // 
            // ucDosingMon15
            // 
            this.ucDosingMon15.BackColor = System.Drawing.Color.White;
            this.ucDosingMon15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon15.Location = new System.Drawing.Point(687, 295);
            this.ucDosingMon15.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon15.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon15.Name = "ucDosingMon15";
            this.ucDosingMon15.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon15.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon15.TabIndex = 42;
            this.ucDosingMon15.TankNum = 15;
            // 
            // ucDosingMon20
            // 
            this.ucDosingMon20.BackColor = System.Drawing.Color.White;
            this.ucDosingMon20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon20.Location = new System.Drawing.Point(3, 441);
            this.ucDosingMon20.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon20.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon20.Name = "ucDosingMon20";
            this.ucDosingMon20.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon20.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon20.TabIndex = 47;
            this.ucDosingMon20.TankNum = 16;
            // 
            // ucDosingMon16
            // 
            this.ucDosingMon16.BackColor = System.Drawing.Color.White;
            this.ucDosingMon16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon16.Location = new System.Drawing.Point(174, 441);
            this.ucDosingMon16.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon16.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon16.Name = "ucDosingMon16";
            this.ucDosingMon16.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon16.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon16.TabIndex = 43;
            this.ucDosingMon16.TankNum = 17;
            // 
            // ucDosingMon17
            // 
            this.ucDosingMon17.BackColor = System.Drawing.Color.White;
            this.ucDosingMon17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon17.Location = new System.Drawing.Point(345, 441);
            this.ucDosingMon17.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon17.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon17.Name = "ucDosingMon17";
            this.ucDosingMon17.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon17.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon17.TabIndex = 44;
            this.ucDosingMon17.TankNum = 18;
            // 
            // ucDosingMon18
            // 
            this.ucDosingMon18.BackColor = System.Drawing.Color.White;
            this.ucDosingMon18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon18.Location = new System.Drawing.Point(516, 441);
            this.ucDosingMon18.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon18.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon18.Name = "ucDosingMon18";
            this.ucDosingMon18.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon18.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon18.TabIndex = 45;
            this.ucDosingMon18.TankNum = 19;
            // 
            // ucDosingMon19
            // 
            this.ucDosingMon19.BackColor = System.Drawing.Color.White;
            this.ucDosingMon19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDosingMon19.Location = new System.Drawing.Point(687, 441);
            this.ucDosingMon19.MaximumSize = new System.Drawing.Size(150, 145);
            this.ucDosingMon19.MinimumSize = new System.Drawing.Size(165, 140);
            this.ucDosingMon19.Name = "ucDosingMon19";
            this.ucDosingMon19.Size = new System.Drawing.Size(165, 140);
            this.ucDosingMon19.Status = Additive_MatDosing_SAPInterface.Model.TankStatus.NotDefine;
            this.ucDosingMon19.TabIndex = 46;
            this.ucDosingMon19.TankNum = 20;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(2, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(574, 81);
            this.label2.TabIndex = 102;
            this.label2.Text = "-";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.chk_atstart);
            this.panel1.Controls.Add(this.bt_stop);
            this.panel1.Controls.Add(this.bt_start);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(454, 605);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(881, 93);
            this.panel1.TabIndex = 37;
            // 
            // chk_atstart
            // 
            this.chk_atstart.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chk_atstart.AutoSize = true;
            this.chk_atstart.Location = new System.Drawing.Point(616, 33);
            this.chk_atstart.Name = "chk_atstart";
            this.chk_atstart.Size = new System.Drawing.Size(71, 22);
            this.chk_atstart.TabIndex = 30;
            this.chk_atstart.Text = "Auto";
            this.chk_atstart.UseVisualStyleBackColor = true;
            this.chk_atstart.CheckedChanged += new System.EventHandler(this.chk_atstart_CheckedChanged);
            // 
            // bt_stop
            // 
            this.bt_stop.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.bt_stop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt_stop.Enabled = false;
            this.bt_stop.FlatAppearance.BorderSize = 0;
            this.bt_stop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.bt_stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_stop.Image = ((System.Drawing.Image)(resources.GetObject("bt_stop.Image")));
            this.bt_stop.Location = new System.Drawing.Point(791, 3);
            this.bt_stop.Name = "bt_stop";
            this.bt_stop.Size = new System.Drawing.Size(85, 85);
            this.bt_stop.TabIndex = 29;
            this.bt_stop.Text = "Stop";
            this.bt_stop.UseVisualStyleBackColor = false;
            this.bt_stop.Click += new System.EventHandler(this.bt_stop_Click);
            // 
            // bt_start
            // 
            this.bt_start.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.bt_start.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt_start.FlatAppearance.BorderSize = 0;
            this.bt_start.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.bt_start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_start.Image = ((System.Drawing.Image)(resources.GetObject("bt_start.Image")));
            this.bt_start.Location = new System.Drawing.Point(693, 3);
            this.bt_start.Name = "bt_start";
            this.bt_start.Size = new System.Drawing.Size(85, 85);
            this.bt_start.TabIndex = 28;
            this.bt_start.Text = "Start";
            this.bt_start.UseVisualStyleBackColor = false;
            this.bt_start.Click += new System.EventHandler(this.bt_start_Click);
            // 
            // chk_logging
            // 
            this.chk_logging.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chk_logging.AutoSize = true;
            this.chk_logging.Checked = true;
            this.chk_logging.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_logging.Location = new System.Drawing.Point(7, 673);
            this.chk_logging.Name = "chk_logging";
            this.chk_logging.Size = new System.Drawing.Size(123, 22);
            this.chk_logging.TabIndex = 38;
            this.chk_logging.Text = "Log to file";
            this.chk_logging.UseVisualStyleBackColor = true;
            this.chk_logging.CheckedChanged += new System.EventHandler(this.chk_logging_CheckedChanged);
            // 
            // dg_tags
            // 
            this.dg_tags.AllowUserToAddRows = false;
            this.dg_tags.AllowUserToResizeRows = false;
            this.dg_tags.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_tags.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(128)))), ((int)(((byte)(185)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_tags.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dg_tags.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_tags.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.orderid,
            this.mat,
            this.target,
            this.current});
            this.dg_tags.EnableHeadersVisualStyles = false;
            this.dg_tags.Location = new System.Drawing.Point(3, 35);
            this.dg_tags.Name = "dg_tags";
            this.dg_tags.RowHeadersVisible = false;
            this.dg_tags.RowHeadersWidth = 51;
            this.dg_tags.RowTemplate.Height = 24;
            this.dg_tags.Size = new System.Drawing.Size(445, 209);
            this.dg_tags.TabIndex = 100;
            this.dg_tags.TabStop = false;
            // 
            // orderid
            // 
            this.orderid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.orderid.HeaderText = "OrderID";
            this.orderid.MinimumWidth = 6;
            this.orderid.Name = "orderid";
            this.orderid.Width = 108;
            // 
            // mat
            // 
            this.mat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mat.HeaderText = "MaterialNo";
            this.mat.MinimumWidth = 6;
            this.mat.Name = "mat";
            this.mat.Width = 139;
            // 
            // target
            // 
            this.target.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.target.HeaderText = "Target";
            this.target.MinimumWidth = 6;
            this.target.Name = "target";
            this.target.Width = 97;
            // 
            // current
            // 
            this.current.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.current.HeaderText = "Current";
            this.current.MinimumWidth = 6;
            this.current.Name = "current";
            this.current.Width = 110;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Copperplate Gothic Bold", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 18);
            this.label1.TabIndex = 101;
            this.label1.Text = "Orders Inprocess";
            // 
            // ucHome
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dg_tags);
            this.Controls.Add(this.chk_logging);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.rtxt_msg);
            this.Controls.Add(this.panel1);
            this.Name = "ucHome";
            this.Size = new System.Drawing.Size(1342, 701);
            this.Load += new System.EventHandler(this.UcHome_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_tags)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtxt_msg;
        private System.Windows.Forms.Label label9;
        private ucDosingMon ucDosingMon1;
        private ucDosingMon ucDosingMon2;
        private ucDosingMon ucDosingMon3;
        private ucDosingMon ucDosingMon4;
        private ucDosingMon ucDosingMon5;
        private ucDosingMon ucDosingMon6;
        private ucDosingMon ucDosingMon7;
        private ucDosingMon ucDosingMon8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private ucDosingMon ucDosingMon9;
        private ucDosingMon ucDosingMon10;
        private ucDosingMon ucDosingMon11;
        private ucDosingMon ucDosingMon12;
        private ucDosingMon ucDosingMon13;
        private ucDosingMon ucDosingMon14;
        private ucDosingMon ucDosingMon15;
        private ucDosingMon ucDosingMon16;
        private ucDosingMon ucDosingMon17;
        private ucDosingMon ucDosingMon18;
        private ucDosingMon ucDosingMon19;
        private ucDosingMon ucDosingMon20;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bt_stop;
        private System.Windows.Forms.Button bt_start;
        private System.Windows.Forms.CheckBox chk_atstart;
        private System.Windows.Forms.CheckBox chk_logging;
        private System.Windows.Forms.DataGridView dg_tags;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderid;
        private System.Windows.Forms.DataGridViewTextBoxColumn mat;
        private System.Windows.Forms.DataGridViewTextBoxColumn target;
        private System.Windows.Forms.DataGridViewTextBoxColumn current;
    }
}
